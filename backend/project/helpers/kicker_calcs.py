import math

import plotly.graph_objs as go  # noqa
from plotly.offline import plot  # noqa

# Constants:
RIDER_MASS = 80  # kg
RIDER_AREA = 1  # m2 for air resistance calcs.
RIDER_DRAG_COEFFICIENT = 1.1  # Wikipedia for air resistance calcs.
AIR_DENSITY = 1.21  # kg/m3
RADIANS_PER_DEGREE = math.pi / 180  # Conversion factor degrees to radians.
KM_PER_HOUR_TO_M_PER_S = 1 / 3.6  # Conversion factor km/h to m/s.
DELTA_T = 0.01  # Time interval for flight calcs.
LENGTH_TO_INITIAL_VEL = 2.0  # Factor to assign initial velocity in km/h based on kicker length.
LANDING_LENGTH_TO_KICKER_LENGTH = 1.5  # Landing length for a given kicker length. Based on experience.
MIN_LANDING_LENGTH = 3  # m. Minimum landing length even for small kickers.
MIN_TAKEOFF_LENGTH = 2  # m. Minimum takeoff length even for small kickers.
TAKEOFF_LENGTH_TO_KICKER_LENGTH = 0.4  # Takeoff length for a given kicker length. Based on experience.
GRAVITY_ACCEL = -9.81  # m/s2
FRICTION_COEFFICIENT_ON_SNOW = 0.05  # https://pdfs.semanticscholar.org/bd55/338fd5080bc0d68f8a5116d454a5bfdfe52e.pdf
MIN_DIST_FROM_PREV_LANDING = 30  # Minimum distance from end of previous landing. Based on experience.
SAFETY_FACTOR_KNUCKLE = 1  # Number of metres rider must clear knuckle by at minimum speed.
SAFETY_FACTOR_END_OF_LANDING = 2  # Number of metres rider lands before end of landing at maximum speed.


def kicker_calcs(kicker):
    # Initial values based on arguments:
    takeoff_height = kicker['takeoff_height']
    takeoff_angle = kicker['takeoff_angle'] * RADIANS_PER_DEGREE
    length = kicker['length']
    takeoff_length = max(length * TAKEOFF_LENGTH_TO_KICKER_LENGTH, MIN_TAKEOFF_LENGTH)
    landing_length = max(length * LANDING_LENGTH_TO_KICKER_LENGTH, MIN_LANDING_LENGTH)
    length_entry_to_exit = takeoff_length + length + landing_length
    landing_angle = kicker['landing_angle'] * RADIANS_PER_DEGREE
    end_of_landing_pos_x = length + (landing_length * math.cos(landing_angle))
    initial_velocity = length * LENGTH_TO_INITIAL_VEL * KM_PER_HOUR_TO_M_PER_S
    pos_x_landing = 0
    min_vel_flight_curve = []
    min_takeoff_velocity = 0
    min_impact_velocity = 0
    min_landing_vel_along_landing = 0
    max_vel_flight_curve = []
    max_takeoff_velocity = 0
    max_impact_velocity = 0
    max_landing_vel_along_landing = 0
    previous_obstacle_total_length = kicker['previous_obstacle_total_length']
    previous_obstacle_dist_from_start = kicker['previous_obstacle_dist_from_start']

    min_curve_x = []
    min_curve_y = []

    results = {
        'takeoff_length': takeoff_length,
        'landing_length': landing_length,
        'min_dist_from_prev_landing': MIN_DIST_FROM_PREV_LANDING,
        'length_entry_to_exit': length_entry_to_exit
    }

    # Iterate flight curve calcs for incrementing velocity until max velocity for the jump is reached (rider
    # lands near end of landing).
    while pos_x_landing <= end_of_landing_pos_x - 1:
        # Set initial state.
        pos_x_landing = 0
        time = 0
        pos_x = 0
        pos_y = takeoff_height
        snow_pos_y = takeoff_height  # The snow level at point of takeoff.
        snow_pos_min = min(takeoff_height, 0)  # The snow level directly after takeoff (kicker-table height).

        flight_curve = [[time, pos_x, pos_y, snow_pos_y]]
        curve_x = [pos_x]
        curve_y = [pos_y]
        snow_y = [snow_pos_y]
        max_takeoff_velocity += 0
        takeoff_velocity = initial_velocity
        vel_x = initial_velocity * math.cos(takeoff_angle)
        vel_y = initial_velocity * math.sin(takeoff_angle)
        accel_x = -0.5 * AIR_DENSITY * RIDER_DRAG_COEFFICIENT * RIDER_AREA * vel_x * vel_x / RIDER_MASS
        accel_y = GRAVITY_ACCEL + 0.5 * AIR_DENSITY * RIDER_DRAG_COEFFICIENT * RIDER_AREA * \
                  -vel_y * abs(vel_y) / RIDER_MASS  # noqa

        # Flight curve calcs for given takeoff speed. Iteration stops when rider lands on the snow.
        while pos_y >= snow_pos_y:
            time += DELTA_T
            vel_x += accel_x * DELTA_T
            vel_y += accel_y * DELTA_T
            pos_x += vel_x * DELTA_T
            pos_y += vel_y * DELTA_T
            accel_x = -0.5 * AIR_DENSITY * RIDER_DRAG_COEFFICIENT * RIDER_AREA * (vel_x ** 2) / RIDER_MASS
            accel_y = GRAVITY_ACCEL + 0.5 * AIR_DENSITY * RIDER_DRAG_COEFFICIENT * RIDER_AREA * \
                      -vel_y * abs(vel_y) / RIDER_MASS  # noqa

            if pos_x <= length:
                snow_pos_y = snow_pos_min * (1 - pos_x / length)
            else:
                snow_pos_y = -math.tan(landing_angle) * (pos_x - length)

            flight_curve.append([time, pos_x, pos_y, snow_pos_y])
            curve_x.append(pos_x)
            curve_y.append(pos_y)
            snow_y.append(snow_pos_y)

        # Calculate landing impact and landing velocity for each takeoff velocity. Update maximum on each
        # iteration.
        pos_x_landing = pos_x
        landing_vel_total = math.sqrt(vel_x * vel_x + vel_y * vel_y)
        landing_vel_angle = math.atan(abs(vel_y / vel_x))
        angle_between_landing_flight_curve = landing_vel_angle - landing_angle
        max_impact_velocity = landing_vel_total * math.sin(angle_between_landing_flight_curve)
        max_landing_vel_along_landing = landing_vel_total * math.cos(angle_between_landing_flight_curve)

        # Update flight curve at maximum velocity.
        max_vel_flight_curve += flight_curve
        max_takeoff_velocity = takeoff_velocity

        # Set minimum velocity values when rider lands 1m past the knuckle.
        if pos_x_landing > length + SAFETY_FACTOR_KNUCKLE and len(min_vel_flight_curve) == 0:
            min_vel_flight_curve = flight_curve
            min_impact_velocity += max_impact_velocity
            min_landing_vel_along_landing += max_landing_vel_along_landing
            min_takeoff_velocity += takeoff_velocity
            min_curve_x += curve_x
            min_curve_y += curve_y

        initial_velocity += 0.5

    # Add results to results dictionary.
    results['min_takeoff_speed'] = min_takeoff_velocity / KM_PER_HOUR_TO_M_PER_S
    results['max_takeoff_speed'] = max_takeoff_velocity / KM_PER_HOUR_TO_M_PER_S
    results['min_entry_speed'] = get_speed_at_start_of_takeoff(results['min_takeoff_speed'],
                                                               kicker['takeoff_angle'], takeoff_length)
    results['max_entry_speed'] = get_speed_at_start_of_takeoff(results['max_takeoff_speed'],
                                                               kicker['takeoff_angle'], takeoff_length)
    results['min_exit_speed'] = get_speed_at_end_of_slope(-min_landing_vel_along_landing / KM_PER_HOUR_TO_M_PER_S,
                                                          kicker['landing_angle'],
                                                          landing_length - SAFETY_FACTOR_KNUCKLE)
    results[
        'max_exit_speed'] = -max_landing_vel_along_landing / KM_PER_HOUR_TO_M_PER_S  # Neglect last few m of landing
    results['min_airtime'] = min_vel_flight_curve[len(min_vel_flight_curve) - 1][0]
    results['max_airtime'] = max_vel_flight_curve[len(max_vel_flight_curve) - 1][0]
    results['min_impact_speed'] = min_impact_velocity / KM_PER_HOUR_TO_M_PER_S
    results['max_impact_speed'] = max_impact_velocity / KM_PER_HOUR_TO_M_PER_S
    results['actual_dist_from_prev_landing'] = get_actual_dist_from_prev_landing(
        kicker['previous_obstacle_exit_speed'],
        results['min_entry_speed'],
        MIN_DIST_FROM_PREV_LANDING,
        kicker['park_slope'],
    )
    results['distance_from_park_start'] = get_actual_dist_from_prev_landing(
        kicker['previous_obstacle_exit_speed'],
        results['min_entry_speed'],
        MIN_DIST_FROM_PREV_LANDING,
        kicker['park_slope'],
    ) + previous_obstacle_total_length + previous_obstacle_dist_from_start
    plot_flight_curve(min_curve_x, min_curve_y, curve_x, curve_y, curve_x, snow_y)
    return results


# Not currently used in app. As future development could be shown with jump details in frontend.
def plot_flight_curve(min_curve_x, min_curve_y, max_curve_x, max_curve_y, kicker_x, kicker_y):
    trace0 = go.Scatter(
        x=max_curve_x,
        y=max_curve_y
    )
    trace1 = go.Scatter(
        x=kicker_x,
        y=kicker_y
    )
    trace2 = go.Scatter(
        x=min_curve_x,
        y=min_curve_y
    )
    data = [trace0, trace1, trace2]

    plot(data, filename='flightcurve.html')


# Argument units: km/h, degrees, m. speed_at_top negative if downhill.
def get_speed_at_end_of_slope(speed_at_start, slope_angle, slope_length):
    angle = slope_angle * RADIANS_PER_DEGREE
    pos_x = 0
    velocity = speed_at_start * KM_PER_HOUR_TO_M_PER_S
    normal_force_on_snow = abs(RIDER_MASS * GRAVITY_ACCEL * math.cos(angle))
    friction_force = FRICTION_COEFFICIENT_ON_SNOW * normal_force_on_snow

    while pos_x < slope_length * math.cos(angle):
        if velocity == 0:
            accel_along_slope = GRAVITY_ACCEL * math.sin(angle)
        else:
            accel_along_slope = GRAVITY_ACCEL * math.sin(angle) - (
                    friction_force * -velocity / abs(velocity) + 0.5 * AIR_DENSITY * RIDER_DRAG_COEFFICIENT *
                    RIDER_AREA * -velocity * abs(velocity)) / RIDER_MASS
        velocity += accel_along_slope * DELTA_T
        pos_x += abs(velocity * math.cos(angle) * DELTA_T)
    return velocity / KM_PER_HOUR_TO_M_PER_S


def get_speed_at_start_of_takeoff(takeoff_speed, takeoff_angle, takeoff_length):
    start_velocity = takeoff_speed  # km/h
    final_velocity = takeoff_speed  # km/h
    while final_velocity <= takeoff_speed:
        final_velocity = get_speed_at_end_of_slope(start_velocity, takeoff_angle, takeoff_length)
        start_velocity += 0.1

    return start_velocity


def get_actual_dist_from_prev_landing(prev_obstacle_exit_speed, current_obstacle_entry_speed, min_dist, slope_angle):
    speed_at_start = -prev_obstacle_exit_speed

    slope_length = min_dist
    speed_at_end_of_slope = 0
    while abs(speed_at_end_of_slope) <= current_obstacle_entry_speed:
        speed_at_end_of_slope = get_speed_at_end_of_slope(speed_at_start, slope_angle, slope_length)
        slope_length += 1
    return slope_length
