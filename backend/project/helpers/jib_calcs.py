def jib_calcs(jib_params):
    takeoff_length = 2  # m. Average/minimum value.
    length_entry_to_exit = takeoff_length + jib_params['length']
    min_entry_speed = 15  # km/h. Assumed value.
    min_exit_speed = min_entry_speed  # km/h. Approximation.
    min_dist_from_prev_landing = 30  # m.
    actual_dist_from_prev_landing = min_dist_from_prev_landing
    distance_from_park_start = jib_params['previous_obstacle_dist_from_start'] + \
                               jib_params['previous_obstacle_total_length'] + actual_dist_from_prev_landing  # noqa

    results = {
        'takeoff_length': takeoff_length,
        'min_entry_speed': min_entry_speed,
        'min_exit_speed': min_exit_speed,
        'min_dist_from_prev_landing': min_dist_from_prev_landing,
        'distance_from_park_start': distance_from_park_start,
        'length_entry_to_exit': length_entry_to_exit,
        'actual_dist_from_prev_landing': actual_dist_from_prev_landing
    }

    return results
