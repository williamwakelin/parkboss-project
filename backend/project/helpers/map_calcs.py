import math

from geopy import distance


def get_park_length_and_slope(mapParams):
    park_start = (mapParams['start_latitude'], mapParams['start_longitude'])
    park_end = (mapParams['end_latitude'], mapParams['end_longitude'])
    park_length_horizontal = distance.distance(park_start, park_end).m

    print(park_length_horizontal)
    start_elevation = mapParams['start_elevation']
    end_elevation = mapParams['end_elevation']
    if park_length_horizontal == 0:
        average_park_slope_radians = 0
    else:
        average_park_slope_radians = math.atan((start_elevation - end_elevation) / park_length_horizontal)

    actual_park_length = park_length_horizontal / math.cos(average_park_slope_radians)

    results = {
        'average_park_slope': math.degrees(average_park_slope_radians),
        'length': actual_park_length
    }

    return results
