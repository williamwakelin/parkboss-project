from django.contrib import admin

from project.models import Park, Map, Kicker, Jib, Quarterpipe, Halfpipe

admin.site.register(Park)
admin.site.register(Map)
admin.site.register(Kicker)
admin.site.register(Jib)
admin.site.register(Quarterpipe)
admin.site.register(Halfpipe)
