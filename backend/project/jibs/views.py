from project.helpers.jib_calcs import jib_calcs
from project.jibs.serializers import JibSerializer
from project.models import Park, Jib, Map, Kicker
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response


class CreateJibView(GenericAPIView):
    '''
    Create a new jib for a given park. ParkID in URL.
    '''

    serializer_class = JibSerializer
    queryset = Jib.objects.all()

    def post(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')

        # Find previous obstacle and park average slope to calculate required distance between obstacles.
        new_jib_position = request.data.get('position')
        if new_jib_position == 1:
            previous_obstacle_exit_speed = 0
            previous_obstacle_dist_from_start = 0
            previous_obstacle_total_length = 0
        else:
            if Kicker.objects.filter(park=current_park, position=new_jib_position - 1).first():
                previous_obstacle = Kicker.objects.filter(
                    park=current_park, position=new_jib_position - 1).first()
            else:
                previous_obstacle = Jib.objects.filter(
                    park=current_park, position=new_jib_position - 1).first()
            previous_obstacle_exit_speed = previous_obstacle.min_exit_speed
            previous_obstacle_dist_from_start = previous_obstacle.distance_from_park_start
            previous_obstacle_total_length = previous_obstacle.length_entry_to_exit

        park_slope = Map.objects.filter(parks=current_park).first().average_park_slope

        jib_calcs_arguments = {
            'length': request.data.get('length'),
            'previous_obstacle_exit_speed': previous_obstacle_exit_speed,
            'previous_obstacle_dist_from_start': previous_obstacle_dist_from_start,
            'previous_obstacle_total_length': previous_obstacle_total_length,
            'park_slope': park_slope
        }

        results = jib_calcs(jib_calcs_arguments)

        new_jib = self.queryset.create(
            park=current_park,
            position=request.data.get('position'),
            distance_from_park_start=results['distance_from_park_start'],
            type=request.data.get('type'),
            length=request.data.get('length'),
            longitude=request.data.get('longitude'),
            latitude=request.data.get('latitude'),
            takeoff_angle=request.data.get('takeoff_angle'),
            takeoff_length=results['takeoff_length'],
            min_entry_speed=results['min_entry_speed'],
            min_exit_speed=results['min_exit_speed'],
            min_dist_from_prev_landing=results['min_dist_from_prev_landing'],
            actual_dist_from_prev_landing=results['actual_dist_from_prev_landing'],
            length_entry_to_exit=results['length_entry_to_exit'],
        )
        return Response(JibSerializer(new_jib).data)


class GetJibByParkIdView(GenericAPIView):
    '''
    get:
    Get a jib by park ID. Park ID taken from URL.

    '''

    serializer_class = JibSerializer
    queryset = Jib.objects.all()

    def get(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        try:
            current_jibs = self.queryset.filter(park=current_park)
        except Jib.DoesNotExist:
            raise NotFound('This park doesn\'t have any jibs yet.')
        return Response(self.serializer_class(current_jibs, many=True).data)


class ListUserJibsView(ListAPIView):
    '''
    get:
    List all the current user's jibs.
    '''

    serializer_class = JibSerializer

    def get_queryset(self):
        current_user = self.request.user
        return Jib.objects.filter(user=current_user).order_by('-length')


class GetUpdateDeleteJibView(GenericAPIView):
    '''
    get:
    Get a jib. Jib ID taken from URL.

    put:
    Update a jib. Jib ID taken from URL.

    delete:
    Delete a jib. Jib ID taken from URL.
    '''

    serializer_class = JibSerializer
    queryset = Jib.objects.all()
    lookup_field = 'id'

    def get(self, request, id, **kwargs):
        try:
            current_jib = self.get_object()
        except Jib.DoesNotExist:
            raise NotFound(f'Jib not found.')
        return Response(self.serializer_class(current_jib).data)

    def put(self, request, **kwargs):
        try:
            current_jib = self.get_object()
        except Jib.DoesNotExist:
            raise NotFound(f'Kicker not found.')
        serializer = self.serializer_class(current_jib, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        current_jib = serializer.save()
        return Response(JibSerializer(current_jib).data)

    def delete(self, request, **kwargs):
        try:
            current_jib = self.get_object()
        except Jib.DoesNotExist:
            raise NotFound(f'Jib not found!')
        current_jib.delete()
        return Response({'detail': f'Jib deleted'})
