from django.urls import path

from project.jibs.views import ListUserJibsView, CreateJibView, GetUpdateDeleteJibView, GetJibByParkIdView

urlpatterns = [
    path('', ListUserJibsView.as_view(), name='list_user_jibs'),
    path('park/<int:park_id>/new/', CreateJibView.as_view(), name='create_jib'),
    path('<int:id>/', GetUpdateDeleteJibView.as_view(), name='get_update_delete_jib'),
    path('park/<int:park_id>/', GetJibByParkIdView.as_view(), name='get_jib_by_park_id'),
]
