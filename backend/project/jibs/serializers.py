from project.models import Jib
from rest_framework import serializers


class JibSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jib
        fields = ['id', 'park', 'position', 'type', 'distance_from_park_start', 'longitude', 'latitude', 'length',
                  'takeoff_angle', 'min_dist_from_prev_landing', 'actual_dist_from_prev_landing',
                  'takeoff_length', 'length_entry_to_exit', 'min_entry_speed', 'min_exit_speed', 'required_snow',
                  'snowcat_hours']
        read_only_fields = ['id', 'park']

    def create(self, validated_data):
        return Jib.objects.create(
            **validated_data
        )
