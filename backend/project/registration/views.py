from django.contrib.auth import get_user_model
from project.registration.serializers import RegistrationSerializer, RegistrationValidationSerializer
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

User = get_user_model()


class RegistrationView(GenericAPIView):
    '''
    post:
    Generate a validation code and send it by email to the user who is signing up.
    '''

    serializer_class = RegistrationSerializer
    permission_classes = [AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Registration validation email sent to {user.email}.'})


class RegistrationValidationView(GenericAPIView):
    '''
    post:
    Check code received from user against code that was sent to them by email. If they match register
    the user by setting the password to the user's given password and setting the user to "active".
    '''

    serializer_class = RegistrationValidationSerializer
    permission_classes = [AllowAny]

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Registration successful for {user.email}.'})
