from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from project.models import UserProfile
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['id', 'user', 'company']
        read_only_fields = ['id', 'user']

    def create(self, validated_data):
        return UserProfile.objects.create(
            user=self.context.get('request').user,
            **validated_data
        )


class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
            raise ValidationError('Email address already used')
        except User.DoesNotExist:
            return email

    def send_validation_email(self, email, validation_code):
        message = EmailMessage(
            to=[email],
            subject='Your ParkBoss Registration',
            body=f'Your code is: {validation_code}'
        )
        message.send()

    def save(self, **kwargs):
        user = User.objects.create_user(
            username=kwargs.get('email'),
            email=kwargs.get('email'),
            is_active=False,
        )
        user.user_profile.generate_validation_code()
        self.send_validation_email(user.email, user.user_profile.validation_code)
        return user


class RegistrationValidationSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()
    password_repeat = serializers.CharField()

    def validate_username(self, username):
        try:
            User.objects.get(username=username)
            raise ValidationError('Username is already taken.')
        except User.DoesNotExist:
            if '@' in username:
                raise ValidationError({'Username cannot contain an @ sign.'})
            return username

    def validate_email(self, email):
        try:
            return User.objects.get(email=email, is_active=False)
        except User.DoesNotExist:
            raise ValidationError('User does not exist or is active.')

    def validate(self, attrs):
        user = attrs.get('email')
        if user.user_profile.validation_code != attrs.get('code'):
            raise ValidationError({'code': 'Code not valid.'})
        if attrs.get('password') != attrs.get('password_repeat'):
            raise ValidationError({'password_repeat': 'Passwords do not match.'})
        return attrs

    def save(self, **kwargs):
        email = kwargs.get('email')
        user = User.objects.get(email=email)
        user.username = kwargs.get('username')
        user.is_active = True
        user.set_password(kwargs.get('password'))
        user.save()
        user.user_profile.validation_code = ''
        user.user_profile.save()
        return user


class PasswordResetValidationSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()
    password_repeat = serializers.CharField()

    def validate_email(self, email):
        try:
            user = User.objects.get(email=email, is_active=True)
            return user.email
        except User.DoesNotExist:
            raise ValidationError('User does not exist or is not active.')

    def validate(self, attrs):
        user = User.objects.get(email=attrs.get('email'))
        if user.user_profile.validation_code != attrs.get('code'):
            raise ValidationError({'code': 'Code not valid.'})
        if attrs.get('password') != attrs.get('password_repeat'):
            raise ValidationError({'password_repeat': 'Passwords do not match.'})
        return attrs

    def save(self, **kwargs):
        email = kwargs.get('email')
        user = User.objects.get(email=email)
        user.set_password(kwargs.get('password'))
        user.save()
        user.user_profile.validation_code = ''
        user.user_profile.save()
        return user
