from django.urls import path

from project.maps.views import ListUserMapsView, GetUpdateDeleteMapView, CreateMapView, GetMapByParkIdView

urlpatterns = [
    path('', ListUserMapsView.as_view(), name='list_user_maps'),
    path('park/<int:park_id>/new/', CreateMapView.as_view(), name='create_map'),
    path('<int:id>/', GetUpdateDeleteMapView.as_view(), name='get_update_delete_map'),
    path('park/<int:park_id>/', GetMapByParkIdView.as_view(), name='get_map_by_park_id'),
]
