from project.helpers.map_calcs import get_park_length_and_slope
from project.maps.serializers import MapSerializer
from project.models import Map, Park
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response


class CreateMapView(GenericAPIView):
    '''
    Create a new map for a given park. ParkID in URL.
    '''

    serializer_class = MapSerializer
    queryset = Map.objects.all()

    def post(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        if self.queryset.filter(parks=current_park):
            return Response({'detail': 'This park already has a map.'}, 400)
        else:

            map_params = {
                'start_longitude': request.data.get('start_longitude'),
                'start_latitude': request.data.get('start_latitude'),
                'start_elevation': request.data.get('start_elevation'),
                'end_longitude': request.data.get('end_longitude'),
                'end_latitude': request.data.get('end_latitude'),
                'end_elevation': request.data.get('end_elevation'),
            }
            results = get_park_length_and_slope(map_params)

            new_map = self.queryset.create(
                parks=current_park,
                center_longitude=request.data.get('center_longitude'),
                center_latitude=request.data.get('center_latitude'),
                zoom_level=request.data.get('zoom_level'),
                start_longitude=request.data.get('start_longitude'),
                start_latitude=request.data.get('start_latitude'),
                start_elevation=request.data.get('start_elevation'),
                end_longitude=request.data.get('end_longitude'),
                end_latitude=request.data.get('end_latitude'),
                end_elevation=request.data.get('end_elevation'),
                length=results['length'],
                average_park_slope=results['average_park_slope'],
            )
            return Response(MapSerializer(new_map).data)


class GetMapByParkIdView(GenericAPIView):
    '''
    get:
    Get a map by park ID. Park ID taken from URL.

    '''

    serializer_class = MapSerializer
    queryset = Map.objects.all()

    def get(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        try:
            current_maps = self.queryset.filter(parks=current_park)
        except Map.DoesNotExist:
            raise NotFound('This park doesn\'t have a map yet.')
        return Response(self.serializer_class(current_maps, many=True).data)


class ListUserMapsView(ListAPIView):
    '''
    get:
    List all the current user's maps.
    '''

    serializer_class = MapSerializer

    def get_queryset(self):
        current_user = self.request.user
        return Map.objects.filter(user=current_user).order_by('-created')


class GetUpdateDeleteMapView(GenericAPIView):
    '''
    get:
    Get a map. Map ID taken from URL.

    put:
    Update a map. Map ID taken from URL.

    delete:
    Delete a map. Map ID taken from URL.
    '''

    serializer_class = MapSerializer
    queryset = Map.objects.all()
    lookup_field = 'id'

    def get(self, request, id, **kwargs):
        try:
            current_map = self.get_object()
        except Map.DoesNotExist:
            raise NotFound(f'Map not found.')
        return Response(self.serializer_class(current_map).data)

    def put(self, request, **kwargs):
        try:
            current_map = self.get_object()
        except Map.DoesNotExist:
            raise NotFound(f'Map not found.')
        serializer = self.serializer_class(current_map, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        current_map = serializer.save()
        return Response(MapSerializer(current_map).data)

    def delete(self, request, **kwargs):
        try:
            current_map = self.get_object()
        except Map.DoesNotExist:
            raise NotFound(f'Map not found!')
        current_map.delete()
        return Response({'detail': f'Map deleted'})
