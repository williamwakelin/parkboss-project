from project.models import Map
from rest_framework import serializers


class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Map
        fields = ['id', 'parks', 'center_longitude', 'center_latitude', 'zoom_level', 'width', 'length',
                  'average_park_slope', 'start_longitude', 'start_latitude', 'start_elevation',
                  'end_longitude', 'end_latitude', 'end_elevation']
        read_only_fields = ['id', 'parks']

    def create(self, validated_data):
        return Map.objects.create(
            **validated_data
        )
