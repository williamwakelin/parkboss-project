from random import choice
from string import ascii_uppercase

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    validation_code_length = 5
    user = models.OneToOneField(
        verbose_name='user',
        related_name='user_profile',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    company = models.CharField(
        verbose_name='company',
        max_length=200,
        null=True,
        blank=True,
    )
    validation_code = models.CharField(
        verbose_name='validation_code',
        max_length=50,
        null=True,
        blank=True,
    )

    def generate_validation_code(self):
        self.validation_code = ''.join(
            choice(ascii_uppercase) for i in range(self.validation_code_length)
        )
        self.save()

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.user_profile.save()


class Park(models.Model):
    name = models.TextField(
        verbose_name='name',
    )
    user = models.ForeignKey(
        verbose_name='user',
        related_name='parks',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    longitude = models.FloatField(
        verbose_name='longitude',
        null=True,
        blank=True,
    )
    latitude = models.FloatField(
        verbose_name='latitude',
        null=True,
        blank=True,
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )


class Map(models.Model):
    center_latitude = models.FloatField(
        verbose_name='center_latitude',
    )
    center_longitude = models.FloatField(
        verbose_name='center_longitude',
    )
    zoom_level = models.FloatField(
        verbose_name='zoom_level',
    )
    start_latitude = models.FloatField(
        verbose_name='start_latitude',
    )
    start_longitude = models.FloatField(
        verbose_name='start_longitude',
    )
    start_elevation = models.FloatField(
        verbose_name='start_elevation',
    )
    end_latitude = models.FloatField(
        verbose_name='end_latitude',
    )
    end_longitude = models.FloatField(
        verbose_name='end_longitude',
    )
    end_elevation = models.FloatField(
        verbose_name='end_elevation',
    )
    width = models.FloatField(
        verbose_name='width',
        null=True,
        blank=True,
    )
    length = models.FloatField(
        verbose_name='length',
    )
    average_park_slope = models.FloatField(
        verbose_name='average_park_slope',
    )
    parks = models.ForeignKey(
        verbose_name='parks',
        related_name='map',
        to=Park,
        on_delete=models.CASCADE,
    )


class Kicker(models.Model):
    position = models.IntegerField(
        verbose_name='position'
    )
    type = models.CharField(
        verbose_name='type',
        max_length=100,
    )
    distance_from_park_start = models.FloatField(
        verbose_name='distance_from_park_start'
    )
    longitude = models.FloatField(
        verbose_name='longitude',
        null=True,
        blank=True,
    )
    latitude = models.FloatField(
        verbose_name='latitude',
        null=True,
        blank=True,
    )
    length = models.FloatField(
        verbose_name='length',
    )
    takeoff_height = models.FloatField(
        verbose_name='takeoff_height',
    )
    takeoff_angle = models.FloatField(
        verbose_name='takeoff_angle',
    )
    takeoff_length = models.FloatField(
        verbose_name='takeoff_length',
        null=True,
        blank=True,
    )
    landing_angle = models.FloatField(
        verbose_name='landing_angle',
    )
    landing_length = models.FloatField(
        verbose_name='landing_length',
        null=True,
        blank=True,
    )
    min_takeoff_speed = models.FloatField(
        verbose_name='min_takeoff_speed',
        null=True,
        blank=True,
    )
    max_takeoff_speed = models.FloatField(
        verbose_name='max_takeoff_speed',
        null=True,
        blank=True,
    )
    min_dist_from_prev_landing = models.FloatField(
        verbose_name='min_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    actual_dist_from_prev_landing = models.FloatField(
        verbose_name='actual_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    length_entry_to_exit = models.FloatField(
        verbose_name='length_entry_to_exit',
        null=True,
        blank=True,
    )
    min_entry_speed = models.FloatField(
        verbose_name='min_entry_speed',
        null=True,
        blank=True,
    )
    max_entry_speed = models.FloatField(
        verbose_name='max_entry_speed',
        null=True,
        blank=True,
    )
    min_exit_speed = models.FloatField(
        verbose_name='min_exit_speed',
        null=True,
        blank=True,
    )
    max_exit_speed = models.FloatField(
        verbose_name='max_exit_speed',
        null=True,
        blank=True,
    )
    min_airtime = models.FloatField(
        verbose_name='min_airtime',
        null=True,
        blank=True,
    )
    max_airtime = models.FloatField(
        verbose_name='max_airtime',
        null=True,
        blank=True,
    )
    min_impact_speed = models.FloatField(
        verbose_name='min_impact_speed',
        null=True,
        blank=True,
    )
    max_impact_speed = models.FloatField(
        verbose_name='max_impact_speed',
        null=True,
        blank=True,
    )
    park = models.ForeignKey(
        verbose_name='park',
        related_name='kickers',
        to=Park,
        on_delete=models.CASCADE,
    )
    required_snow = models.FloatField(
        verbose_name='required_snow',
        null=True,
        blank=True,
    )
    snowcat_hours = models.FloatField(
        verbose_name='snowcat_hours',
        null=True,
        blank=True,
    )


class Jib(models.Model):
    position = models.IntegerField(
        verbose_name='position'
    )
    type = models.CharField(
        verbose_name='type',
        max_length=100,
    )
    distance_from_park_start = models.FloatField(
        verbose_name='distance_from_park_start'
    )
    longitude = models.FloatField(
        verbose_name='longitude',
        null=True,
        blank=True,
    )
    latitude = models.FloatField(
        verbose_name='latitude',
        null=True,
        blank=True,
    )
    length = models.FloatField(
        verbose_name='length',
    )
    takeoff_angle = models.FloatField(
        verbose_name='takeoff_angle',
    )
    takeoff_length = models.FloatField(
        verbose_name='takeoff_length',
        null=True,
        blank=True,
    )
    actual_dist_from_prev_landing = models.FloatField(
        verbose_name='actual_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    length_entry_to_exit = models.FloatField(
        verbose_name='length_entry_to_exit',
        null=True,
        blank=True,
    )
    min_entry_speed = models.FloatField(
        verbose_name='entry_speed',
        null=True,
        blank=True,
    )
    min_exit_speed = models.FloatField(
        verbose_name='landing_speed',
        null=True,
        blank=True,
    )
    min_dist_from_prev_landing = models.FloatField(
        verbose_name='min_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    park = models.ForeignKey(
        verbose_name='park',
        related_name='jibs',
        to=Park,
        on_delete=models.CASCADE,
    )
    required_snow = models.FloatField(
        verbose_name='required_snow',
        null=True,
        blank=True,
    )
    snowcat_hours = models.FloatField(
        verbose_name='snowcat_hours',
        null=True,
        blank=True,
    )


class Quarterpipe(models.Model):
    position = models.IntegerField(
        verbose_name='position'
    )
    distance_from_park_start = models.FloatField(
        verbose_name='distance_from_park_start'
    )
    longitude = models.FloatField(
        verbose_name='longitude',
        null=True,
        blank=True,
    )
    latitude = models.FloatField(
        verbose_name='latitude',
        null=True,
        blank=True,
    )
    takeoff_height = models.FloatField(
        verbose_name='takeoff_height',
    )
    min_dist_from_prev_landing = models.FloatField(
        verbose_name='min_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    actual_dist_from_prev_landing = models.FloatField(
        verbose_name='actual_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    min_entry_speed = models.FloatField(
        verbose_name='min_entry_speed',
        null=True,
        blank=True,
    )
    max_entry_speed = models.FloatField(
        verbose_name='max_entry_speed',
        null=True,
        blank=True,
    )
    min_jumpheight = models.FloatField(
        verbose_name='min_jumpheight',
        null=True,
        blank=True,
    )
    max_jumpheight = models.FloatField(
        verbose_name='max_jumpheight',
        null=True,
        blank=True,
    )
    park = models.ForeignKey(
        verbose_name='park',
        related_name='quarterpipes',
        to=Park,
        on_delete=models.CASCADE,
    )
    required_snow = models.FloatField(
        verbose_name='required_snow',
        null=True,
        blank=True,
    )
    snowcat_hours = models.FloatField(
        verbose_name='snowcat_hours',
        null=True,
        blank=True,
    )


class Halfpipe(models.Model):
    position = models.IntegerField(
        verbose_name='position'
    )
    distance_from_park_start = models.FloatField(
        verbose_name='distance_from_park_start'
    )
    actual_dist_from_prev_landing = models.FloatField(
        verbose_name='actual_dist_from_prev_landing',
        null=True,
        blank=True,
    )
    longitude = models.FloatField(
        verbose_name='longitude',
        null=True,
        blank=True,
    )
    latitude = models.FloatField(
        verbose_name='latitude',
        null=True,
        blank=True,
    )
    wall_height = models.FloatField(
        verbose_name='wall_height',
    )
    length = models.FloatField(
        verbose_name='length',
        null=True,
        blank=True,
    )
    slope = models.FloatField(
        verbose_name='slope',
        null=True,
        blank=True,
    )
    width = models.FloatField(
        verbose_name='width',
        null=True,
        blank=True,
    )
    park = models.ForeignKey(
        verbose_name='park',
        related_name='halfpipes',
        to=Park,
        on_delete=models.CASCADE,
    )
    required_snow = models.FloatField(
        verbose_name='required_snow',
        null=True,
        blank=True,
    )
    snowcat_hours = models.FloatField(
        verbose_name='snowcat_hours',
        null=True,
        blank=True,
    )
