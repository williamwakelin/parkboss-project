from project.models import Park
from project.parks.serializers import ParkSerializer
from rest_framework.exceptions import NotFound
from rest_framework.generics import CreateAPIView, GenericAPIView, ListAPIView
from rest_framework.response import Response


class AddParkView(CreateAPIView):
    '''
    Create a new park project by current logged-in user.
    '''

    serializer_class = ParkSerializer
    queryset = Park.objects.all()


class ListUserParksView(ListAPIView):
    '''
    get:
    List all the current user's park projects.
    '''

    serializer_class = ParkSerializer

    def get_queryset(self):
        current_user = self.request.user
        return Park.objects.filter(user=current_user).order_by('-created')


class GetUpdateDeleteParkView(GenericAPIView):
    '''
    get:
    Get a park. Park ID taken from URL.

    put:
    Update a park. Park ID taken from URL.

    delete:
    Delete a park. Park ID taken from URL.
    '''

    serializer_class = ParkSerializer
    queryset = Park.objects.all()
    lookup_field = 'id'

    def get(self, request, id, **kwargs):
        try:
            park = self.get_object()
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        return Response(self.serializer_class(park).data)

    def put(self, request, **kwargs):
        try:
            park = self.get_object()
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        serializer = self.serializer_class(park, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        park = serializer.save()
        return Response(ParkSerializer(park).data)

    def delete(self, request, **kwargs):
        try:
            park = self.get_object()
        except Park.DoesNotExist:
            raise NotFound(f'Park not found!')
        park.delete()
        return Response({'detail': f'{park.name} deleted'})
