from django.urls import path

from project.parks.views import ListUserParksView, AddParkView, GetUpdateDeleteParkView

urlpatterns = [
    path('', ListUserParksView.as_view(), name='list_user_parks'),
    path('new/', AddParkView.as_view(), name='add_park'),
    path('<int:id>/', GetUpdateDeleteParkView.as_view(), name='get_update_delete_park'),
]
