from project.models import Park
from rest_framework import serializers


class ParkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Park
        fields = ['id', 'name', 'user', 'longitude', 'latitude', 'map', 'jibs', 'kickers', 'halfpipes',
                  'quarterpipes', 'created']
        read_only_fields = ['id', 'created', 'user', 'longitude', 'latitude', 'map', 'jibs', 'kickers',
                            'halfpipes', 'quarterpipes', 'created']

    def create(self, validated_data):
        return Park.objects.create(
            user=self.context.get('request').user,
            **validated_data
        )
