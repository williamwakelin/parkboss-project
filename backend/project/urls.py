"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('backend/admin/', admin.site.urls),
    path('backend/auth/', include('project.authorization.urls')),
    path('backend/registration/', include('project.registration.urls')),
    path('backend/parks/', include('project.parks.urls')),
    path('backend/maps/', include('project.maps.urls')),
    path('backend/kickers/', include('project.kickers.urls')),
    path('backend/jibs/', include('project.jibs.urls')),
]
