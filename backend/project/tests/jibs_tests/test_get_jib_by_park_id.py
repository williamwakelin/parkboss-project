from django.contrib.auth import get_user_model
from project.models import Park, Jib
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class GetJibByParkIdTest(MasterTestWrapper.MasterTests):
    endpoint = 'get_jib_by_park_id'
    methods = ['GET']

    def get_kwargs(self):
        return {
            'park_id': Park.objects.first().id,
        }

    def setUp(self):
        super().setUp()
        for i in range(2):
            Park.objects.create(
                user=self.user,
                name=f'Test Snowpark {i}'
            )
        for i in range(2):
            Jib.objects.create(
                park=Park.objects.first(),
                type='down_rail',
                longitude=i,
                latitude=i,
                length=i,
                takeoff_angle=i,
            )
        Jib.objects.create(
            park=Park.objects.last(),
            type='down_rail',
            longitude=5,
            latitude=5,
            length=5,
            takeoff_angle=5,
        )

    def get_url_and_login(self, *args, **kwargs):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_jib_count(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
