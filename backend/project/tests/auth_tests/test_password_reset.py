from django.contrib.auth import get_user_model
from django.core import mail
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class PasswordResetEmailTests(MasterTestWrapper.BasicMasterTests):
    endpoint = 'password_reset'
    methods = ['POST']

    def get_url_and_login(self):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url()
        self.authorize()
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_password_reset(self):
        url = self.get_url()
        self.authorize()
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json().get('detail'),
            'Password reset email sent to test@test.com.'
        )

    def test_new_validation_code_created(self):
        url = self.get_url()
        self.authorize()
        old_validation_code = self.user.user_profile.validation_code
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(User.objects.get(pk=self.user.id).user_profile.validation_code, old_validation_code)

    def test_password_email_sent(self):
        url = self.get_url()
        self.authorize()
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(User.objects.get(pk=self.user.id).user_profile.validation_code,
                      mail.outbox[0].body
                      )


class RegistrationValidationTests(MasterTestWrapper.BasicMasterTests):
    endpoint = 'validate_password_reset'
    methods = ['POST']

    def setUp(self):
        super().setUp()
        self.body = {
            'email': self.user.email,
            'username': self.user.username,
            'password': 'new_password',
            'password_repeat': 'new_password',
            'code': 12345,
        }
        self.user.password = 'old_password'
        self.user.user_profile.validation_code = 12345
        self.user.user_profile.save()

    def test_password_reset_validation(self):
        url = self.get_url()
        self.authorize()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json().get('detail'),
            'Password reset for test_user.'
        )

    def test_password_reset_no_code(self):
        url = self.get_url()
        self.authorize()
        self.body.update({
            'code': '',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
