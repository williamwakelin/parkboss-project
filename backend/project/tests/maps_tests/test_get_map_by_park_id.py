from django.contrib.auth import get_user_model
from project.models import Map, Park
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class GetMapByParkIdTest(MasterTestWrapper.MasterTests):
    endpoint = 'get_map_by_park_id'
    methods = ['GET']

    def get_kwargs(self):
        return {
            'park_id': Park.objects.first().id,
        }

    def setUp(self):
        super().setUp()
        for i in range(2):
            Park.objects.create(
                user=self.user,
                name=f'Test Snowpark {i}'
            )
        for i in range(2):
            Map.objects.create(
                parks=Park.objects.first(),
                center_longitude=i,
                center_latitude=i,
                width=i,
                length=i
            )
        Map.objects.create(
            parks=Park.objects.last(),
            center_longitude=5,
            center_latitude=5,
            width=5,
            length=5
        )

    def get_url_and_login(self, *args, **kwargs):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_map_count(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
