from django.contrib.auth import get_user_model
from project.models import Map, Park
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class GetUpdateDeleteMapTest(MasterTestWrapper.MasterTests):
    endpoint = 'get_update_delete_map'
    methods = ['GET', 'PUT', 'DELETE']

    def get_kwargs(self):
        return {
            'id': Map.objects.first().id,
        }

    def setUp(self):
        super().setUp()
        test_park = Park.objects.create(
            user=self.user,
            name=f'Test Snowpark'
        )
        for i in range(2):
            Map.objects.create(
                parks=test_park,
                center_longitude=i,
                center_latitude=i,
                width=i,
                length=i
            )

    def get_url_and_login(self, *args, **kwargs):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_map_id_and_width(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('id'), Map.objects.first().id)
        self.assertEqual(response.data.get('width'), 0)

    def test_if_map_updated(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.put(url, {"width": 5000}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Map.objects.count(), 2)
        self.assertEqual(Map.objects.filter(id=Map.objects.first().id).get().width, 5000)

    def test_if_map_deleted_on_delete(self):
        url = self.get_url_and_login(**self.kwargs)
        id_to_delete = self.get_kwargs()['id']
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Map.objects.count(), 1)
        self.assertFalse(Map.objects.filter(id=id_to_delete))
