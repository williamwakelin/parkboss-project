from project.helpers.kicker_calcs import kicker_calcs
from rest_framework.test import APITestCase


class CalcsTests(APITestCase):
    endpoint = None
    methods = []
    kwargs = {}

    test_kicker = {
        'type': 'Roller',
        'longitude': 5,
        'latitude': 5,
        'length': 15,
        'takeoff_height': 2.5,
        'takeoff_angle': 26,
        'landing_angle': 32,
        'previous_obstacle_exit_speed': 10,
        'previous_obstacle_dist_from_start': 20,
        'previous_obstacle_total_length': 20,
        'park_slope': 15,
    }

    kicker_calcs(test_kicker)
