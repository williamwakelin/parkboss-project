from project.helpers.map_calcs import get_park_length_and_slope

mapParams = {
    'start_latitude': 25.18,
    'start_longitude': -2.1645764930800815,
    'end_latitude': 25.187480842880853,
    'end_longitude': -2.1645764930800815,
    'start_elevation': 1000,
    'end_elevation': 20,
}

get_park_length_and_slope(mapParams)
