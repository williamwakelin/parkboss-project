from django.contrib.auth import get_user_model
from django.core import mail
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class RegistrationTests(MasterTestWrapper.BasicMasterTests):
    endpoint = 'registration'
    methods = ['POST']
    body = {
        'email': 'something@email.com',
    }

    def get_url_and_login(self):
        url = self.get_url()
        self.authorize()
        return url

    def test_registration(self):
        url = self.get_url()
        response = self.client.post(url, {
            'email': 'something@email.com',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json().get('detail'),
            'Registration validation email sent to something@email.com.'
        )

    def test_registration_db_creation(self):
        url = self.get_url()
        response = self.client.post(url, {
            'email': 'something@email.com',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.get(email='something@email.com').username,
                         'something@email.com')

    def test_registration_email_sent(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        user = User.objects.get(email='something@email.com')
        self.assertIn(user.user_profile.validation_code,
                      mail.outbox[0].body
                      )


class RegistrationValidationTests(MasterTestWrapper.BasicMasterTests):
    endpoint = 'registration_validation'
    methods = ['POST']

    def setUp(self):
        super().setUp()
        self.body = {
            'email': 'something@email.com',
            'username': 'something',
            'password': 'supersecure',
            'password_repeat': 'supersecure',
            'code': 12345,
        }
        self.registration_user = User.objects.create_user(
            username='something@email.com',
            email='something@email.com',
            is_active=False,
        )
        self.registration_user.user_profile.validation_code = 12345
        self.registration_user.save()

    def test_registration_validation(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json().get('detail'),
            f'Registration successful for something@email.com.'
        )

    def test_registration_user_active(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(User.objects.get(email='something@email.com').is_active)

    def test_registration_no_code(self):
        url = self.get_url()
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.get(email='something@email.com').user_profile.validation_code, '')

    def test_registration_wrong_code(self):
        url = self.get_url()
        self.body.update({
            'code': 'wrong_code',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json().get('code')[0], 'Code not valid.')

    def test_password_not_match(self):
        url = self.get_url()
        self.body.update({
            'password_repeat': 'wrong_password',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json().get('password_repeat')[0], 'Passwords do not match.')

    def test_username_taken(self):
        url = self.get_url()
        self.body.update({
            'username': 'test_user',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json().get('username')[0], 'Username is already taken.')

    def test_registration_password_user_does_not_exist(self):
        url = self.get_url()
        self.body.update({
            'email': 'nonexistent@email.com',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json().get('email')[0], 'User does not exist or is active.')

    def test_registration_user_already_active(self):
        url = self.get_url()
        self.body.update({
            'email': 'test@test.com',
        })
        response = self.client.post(url, self.body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json().get('email')[0], 'User does not exist or is active.')
