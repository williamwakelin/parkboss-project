from django.contrib.auth import get_user_model
from project.models import Park
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class GetUpdateDeleteParkTest(MasterTestWrapper.MasterTests):
    endpoint = 'get_update_delete_park'
    methods = ['GET', 'PUT', 'DELETE']

    def get_kwargs(self):
        return {
            'id': Park.objects.first().id,
        }

    def setUp(self):
        super().setUp()
        for i in range(5):
            Park.objects.create(
                user=self.user,
                name=f'Snowpark {i}'
            )

    def get_url_and_login(self, *args, **kwargs):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_park_id_and_name(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('id'), Park.objects.first().id)
        self.assertEqual(response.data.get('name'), 'Snowpark 0')

    def test_if_park_updated(self):
        url = self.get_url_and_login(**self.kwargs)
        response = self.client.put(url, {"name": "New Name"}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Park.objects.count(), 5)
        self.assertEqual(Park.objects.filter(id=Park.objects.first().id).get().name, 'New Name')

    def test_if_park_deleted_on_delete(self):
        url = self.get_url_and_login(**self.kwargs)
        id_to_delete = self.get_kwargs()['id']
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(Park.objects.count(), 4)
        self.assertFalse(Park.objects.filter(id=id_to_delete))
