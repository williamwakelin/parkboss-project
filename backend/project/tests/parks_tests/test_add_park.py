from django.contrib.auth import get_user_model
from project.models import Park
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class AddPostTests(MasterTestWrapper.MasterTests):
    endpoint = 'add_park'
    methods = ['POST']

    def get_url_and_login(self):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url()
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_add_park(self):
        url = self.get_url()
        self.authorize()
        response = self.client.post(url, {"name": "Snowpark Norikura"}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Park.objects.count(), 1)
        self.assertEqual(Park.objects.first().name, 'Snowpark Norikura')
