from django.contrib.auth import get_user_model
from project.models import Park
from project.tests.master_tests import MasterTestWrapper
from rest_framework import status

User = get_user_model()


class FeedTests(MasterTestWrapper.MasterTests):
    endpoint = 'list_user_parks'
    methods = ['GET']

    def setUp(self):
        super().setUp()
        for i in range(2):
            Park.objects.create(
                user=self.user,
                name=f'Snowpark {i} {self.user.username}',
            )
        for i in range(2):
            Park.objects.create(
                user=self.other_user,
                name=f'Snowpark {i} {self.other_user.username}',
            )

    def get_url_and_login(self):
        url = self.get_url()
        self.authorize()
        return url

    def test_method_not_allowed(self):
        url = self.get_url()
        self.authorize()
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauthorized(self):
        url = self.get_url()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_park_count(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_park_user(self):
        url = self.get_url()
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('name'), 'Snowpark 1 test_user')
