from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from project.registration.serializers import UserProfileSerializer, PasswordResetValidationSerializer
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

user = get_user_model()


class SendPasswordResetEmailView(GenericAPIView):
    '''
    post:
    Generate a validation code and send it by email to the user who is changing their password.
    '''

    serializer_class = UserProfileSerializer

    def post(self, request, **kwargs):
        request.user.user_profile.generate_validation_code()
        message = EmailMessage(
            to=[request.user.email],
            subject='Reset password on ParkBoss',
            body=f'Your password reset code is: {request.user.user_profile.validation_code}'
        )
        message.send()
        return Response({'detail': f'Password reset email sent to {request.user.email}.'})


class ValidatePasswordResetCodeView(GenericAPIView):
    '''
    post:
    Check code received from user against code that was sent to them and if they match reset user's
    password to the new given password.
    '''

    serializer_class = PasswordResetValidationSerializer

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            current_user = serializer.save(**serializer.validated_data)
            return Response({'detail': f'Password reset for {current_user.username}.'})
