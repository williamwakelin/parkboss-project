from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from project.authorization.views import SendPasswordResetEmailView, ValidatePasswordResetCodeView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView)

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_refresh'),
    path('password-reset/', SendPasswordResetEmailView.as_view(), name='password_reset'),
    path('password-reset/validate/', ValidatePasswordResetCodeView.as_view(), name='validate_password_reset'),
]

urlpatterns += staticfiles_urlpatterns()
