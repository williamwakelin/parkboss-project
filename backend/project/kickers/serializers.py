from project.models import Kicker
from rest_framework import serializers


class KickerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kicker
        fields = ['id', 'park', 'position', 'type', 'distance_from_park_start', 'longitude', 'latitude', 'length',
                  'takeoff_height', 'takeoff_angle', 'takeoff_length', 'landing_angle', 'landing_length',
                  'min_takeoff_speed', 'max_takeoff_speed', 'min_dist_from_prev_landing',
                  'actual_dist_from_prev_landing', 'min_entry_speed', 'max_entry_speed', 'min_exit_speed',
                  'max_exit_speed', 'min_airtime', 'max_airtime', 'min_impact_speed', 'max_impact_speed',
                  'required_snow', 'snowcat_hours', 'length_entry_to_exit']
        read_only_fields = ['id', 'park']

    def create(self, validated_data):
        return Kicker.objects.create(
            **validated_data
        )
