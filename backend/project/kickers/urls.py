from django.urls import path

from project.kickers.views import ListUserKickersView, CreateKickerView, GetUpdateDeleteKickerView, \
    GetKickerByParkIdView

urlpatterns = [
    path('', ListUserKickersView.as_view(), name='list_user_kickers'),
    path('park/<int:park_id>/new/', CreateKickerView.as_view(), name='create_kicker'),
    path('<int:id>/', GetUpdateDeleteKickerView.as_view(), name='get_update_delete_kicker'),
    path('park/<int:park_id>/', GetKickerByParkIdView.as_view(), name='get_kicker_by_park_id'),
]
