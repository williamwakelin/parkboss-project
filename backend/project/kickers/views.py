from project.helpers.kicker_calcs import kicker_calcs
from project.kickers.serializers import KickerSerializer
from project.models import Park, Kicker, Jib, Map
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response


class CreateKickerView(GenericAPIView):
    '''
    Create a new kicker for a given park. ParkID in URL.
    '''

    serializer_class = KickerSerializer
    queryset = Kicker.objects.all()

    def post(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')

        # Find previous obstacle and park average slope to calculate required distance between obstacles.
        new_kicker_position = request.data.get('position')
        print(new_kicker_position)
        if new_kicker_position == 1:
            previous_obstacle_exit_speed = 0
            previous_obstacle_dist_from_start = 0
            previous_obstacle_total_length = 0
        else:
            if Kicker.objects.filter(park=current_park, position=new_kicker_position - 1).first():
                previous_obstacle = Kicker.objects.filter(
                    park=current_park, position=new_kicker_position - 1).first()
            else:
                previous_obstacle = Jib.objects.filter(
                    park=current_park, position=new_kicker_position - 1).first()
            print('previous obst', previous_obstacle)
            previous_obstacle_exit_speed = previous_obstacle.min_exit_speed
            previous_obstacle_dist_from_start = previous_obstacle.distance_from_park_start
            previous_obstacle_total_length = previous_obstacle.length_entry_to_exit

        park_slope = Map.objects.filter(parks=current_park).first().average_park_slope

        kicker_calc_arguments = {
            'length': request.data.get('length'),
            'takeoff_height': request.data.get('takeoff_height'),
            'takeoff_angle': request.data.get('takeoff_angle'),
            'landing_angle': request.data.get('landing_angle'),
            'previous_obstacle_exit_speed': previous_obstacle_exit_speed,
            'previous_obstacle_dist_from_start': previous_obstacle_dist_from_start,
            'previous_obstacle_total_length': previous_obstacle_total_length,
            'park_slope': park_slope
        }

        results = kicker_calcs(kicker_calc_arguments)

        new_kicker = self.queryset.create(
            park=current_park,
            position=request.data.get('position'),
            distance_from_park_start=results['distance_from_park_start'],
            type=request.data.get('type'),
            length=request.data.get('length'),
            longitude=request.data.get('longitude'),
            latitude=request.data.get('latitude'),
            takeoff_height=request.data.get('takeoff_height'),
            takeoff_angle=request.data.get('takeoff_angle'),
            takeoff_length=results['takeoff_length'],
            landing_angle=request.data.get('landing_angle'),
            landing_length=results['landing_length'],
            min_takeoff_speed=results['min_takeoff_speed'],
            max_takeoff_speed=results['max_takeoff_speed'],
            min_entry_speed=results['min_entry_speed'],
            max_entry_speed=results['max_entry_speed'],
            min_exit_speed=results['min_exit_speed'],
            max_exit_speed=results['max_exit_speed'],
            min_airtime=results['min_airtime'],
            max_airtime=results['max_airtime'],
            min_impact_speed=results['min_impact_speed'],
            max_impact_speed=results['max_impact_speed'],
            min_dist_from_prev_landing=results['min_dist_from_prev_landing'],
            actual_dist_from_prev_landing=results['actual_dist_from_prev_landing'],
            length_entry_to_exit=results['length_entry_to_exit'],
        )
        return Response(KickerSerializer(new_kicker).data)


class GetKickerByParkIdView(GenericAPIView):
    '''
    get:
    Get a kicker by park ID. Park ID taken from URL.

    '''

    serializer_class = KickerSerializer
    queryset = Kicker.objects.all()

    def get(self, request, park_id, **kwargs):
        try:
            current_park = Park.objects.get(id=park_id)
        except Park.DoesNotExist:
            raise NotFound(f'Park not found.')
        try:
            current_kickers = self.queryset.filter(park=current_park)
        except Kicker.DoesNotExist:
            raise NotFound('This park doesn\'t have any kickers yet.')
        return Response(self.serializer_class(current_kickers, many=True).data)


class ListUserKickersView(ListAPIView):
    '''
    get:
    List all the current user's kickers.
    '''

    serializer_class = KickerSerializer

    def get_queryset(self):
        current_user = self.request.user
        return Kicker.objects.filter(user=current_user).order_by('-length')


class GetUpdateDeleteKickerView(GenericAPIView):
    '''
    get:
    Get a kicker. Kicker ID taken from URL.

    put:
    Update a kicker. Kicker ID taken from URL.

    delete:
    Delete a kicker. Kicker ID taken from URL.
    '''

    serializer_class = KickerSerializer
    queryset = Kicker.objects.all()
    lookup_field = 'id'

    def get(self, request, id, **kwargs):
        try:
            current_kicker = self.get_object()
        except Kicker.DoesNotExist:
            raise NotFound(f'Kicker not found.')
        return Response(self.serializer_class(current_kicker).data)

    def put(self, request, **kwargs):
        try:
            current_kicker = self.get_object()
        except Kicker.DoesNotExist:
            raise NotFound(f'Kicker not found.')
        serializer = self.serializer_class(current_kicker, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        current_kicker = serializer.save()
        return Response(KickerSerializer(current_kicker).data)

    def delete(self, request, **kwargs):
        try:
            current_kicker = self.get_object()
        except Kicker.DoesNotExist:
            raise NotFound(f'Kicker not found!')
        current_kicker.delete()
        return Response({'detail': f'Kicker deleted'})
