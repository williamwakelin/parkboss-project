export const SELECT_PARK = 'park/select';

export const selectParkAction = (parkId) => {
  return {
    type: SELECT_PARK,
    payload: {
      currentParkId: parkId,
    }
  }
} 

export const fetchLocalCurrentPark = () => (dispatch) => {
  const currentParkStr = localStorage.getItem('currentPark');
  if (currentParkStr) {
    const parkId = parseInt(JSON.parse(currentParkStr));
    dispatch(selectParkAction(parkId)); 
  }
}




