import { baseUrlBackend } from '../../constants';

export const GET_JIBS = 'jibs/get';
export const CREATE_JIB = 'jibs/create';
export const DELETE_JIB = 'jibs/delete';

export const getJibsAction = (jibs) => {
  return {
    type: GET_JIBS,
    payload: {
      jibs: jibs,
    }
  }
} 

export const createJibAction = (jib) => {
  return {
    type: CREATE_JIB,
    payload: {
      jib: jib,
    }
  }
} 

export const deleteJibAction = (jib) => {
  return {
    type: DELETE_JIB,
    payload: {
      jib: jib,
    }
  }
} 

// Below here is middleware.
export const getJibs = (parkId) => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/jibs/park/${parkId}/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'GET'
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(jibs => {
      dispatch(getJibsAction(jibs));
    });
}

export const createJib = (jibParams) => (dispatch, getState) =>  {
  const parkId = getState().currentPark;
  jibParams.position = getState().obstacles.length + 1;
  const url = `${baseUrlBackend}/jibs/park/${parkId}/new/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'POST',
    body: JSON.stringify(jibParams)
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(jib => {
      dispatch(createJibAction(jib));
    });
}

export const deleteJib = (jib) => (dispatch, getState) =>  {
  const jibId = jib.id;
  const url = `${baseUrlBackend}/jibs/${jibId}/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'DELETE',
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(jib => {
      dispatch(deleteJibAction(jib));
    });
}