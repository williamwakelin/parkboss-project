import { baseUrlBackend } from '../../constants';

export const GET_KICKERS = 'kickers/get';
export const CREATE_KICKER = 'kickers/create';
export const DELETE_KICKER = 'kickers/delete';
const MAX_IMPACT_SPEED = 18; // km/h

export const getKickersAction = (kickers) => {
  return {
    type: GET_KICKERS,
    payload: {
      kickers: kickers,
    }
  }
} 

export const createKickerAction = (kicker) => {
  return {
    type: CREATE_KICKER,
    payload: {
      kicker: kicker,
    }
  }
} 

export const deleteKickerAction = (kicker) => {
  return {
    type: DELETE_KICKER,
    payload: {
      kicker: kicker,
    }
  }
} 


// Below here is middleware.
export const getKickers = (parkId) => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/kickers/park/${parkId}/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'GET'
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(kickers => {
      dispatch(getKickersAction(kickers));
    });
}

export const createKicker = (kickerParams) => (dispatch, getState) =>  {
  const parkId = getState().currentPark;
  kickerParams.position = getState().obstacles.length + 1;
  const url = `${baseUrlBackend}/kickers/park/${parkId}/new/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'POST',
    body: JSON.stringify(kickerParams)
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(kicker => {
      dispatch(createKickerAction(kicker));
      if (kicker.min_impact_speed > MAX_IMPACT_SPEED) {
        alert('Watch your knees! That landing is a little flat. ' +
          'If your landing is already steeper than 30 degrees consider ' +
          'reducing the takeoff height or takeoff angle.'
        );
      }
    });
}

export const deleteKicker = (kicker) => (dispatch, getState) =>  {
  const kickerId = kicker.id;
  const url = `${baseUrlBackend}/kickers/${kickerId}/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'DELETE',
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(kicker => {
      dispatch(deleteKickerAction(kicker));
    });
}