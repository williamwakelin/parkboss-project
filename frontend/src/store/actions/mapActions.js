import { baseUrlBackend } from '../../constants';

export const GET_MAPS = 'maps/get';
export const CREATE_MAP = 'maps/create';


export const getMapsAction = (maps) => {
  return {
    type: GET_MAPS,
    payload: {
      maps: maps,
    }
  }
} 

export const createMapAction = (map) => {
  return {
    type: CREATE_MAP,
    payload: {
      map: map,
    }
  }
} 

// Below here is middleware.
export const getMaps = (parkId) => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/maps/park/${parkId}/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'GET',
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(maps => {
      dispatch(getMapsAction(maps));
    });
}

export const fetchLocalMaps = () => (dispatch) => {
  const mapStr = localStorage.getItem('map');
  if (mapStr) {
    const map = JSON.parse(mapStr);
    dispatch(getMapsAction(map)); 
  }
}

export const getElevations = (mapParams) => (dispatch, getState) =>  {
  const elevationKey = process.env.REACT_APP_ELEVATON_KEY;

  const url = `https://elevation-api.io/api/elevation?points=(${mapParams.startLat},${mapParams.startLng}),(${mapParams.endLat},${mapParams.endLng})&resolution=90&key=${elevationKey}`
  const options = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': '*',
    }
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(elevations => {
      mapParams = {
        center_latitude: mapParams.centerLat,
        center_longitude: mapParams.centerLng,
        start_latitude: mapParams.startLat,
        start_longitude: mapParams.startLng,
        end_latitude: mapParams.endLat,
        end_longitude: mapParams.endLng,
        zoom_level: mapParams.zoom,
        start_elevation: elevations.elevations[0].elevation,
        end_elevation: elevations.elevations[1].elevation, 
      }
      if (mapParams.start_elevation === mapParams.end_elevation) {
        console.log('Your chosen area is flat! That\'s not going to work. Try again.');
      } else {
        dispatch(createMap(mapParams));
      }
    }).catch((err) => {
      alert('We were unable to get all the data we need from another website. Try drawing your park on the map again.');
    });
}

export const createMap = (mapParams) => (dispatch, getState) =>  {
  const parkId = getState().currentPark;
  
  const url = `${baseUrlBackend}/maps/park/${parkId}/new/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'POST',
    body: JSON.stringify(mapParams)
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(map => {
      if (map.average_park_slope < 10) {
        alert('The area you\'ve chosen for your park is a little flat. Ideally it should be between 10 and 20 deg.');
      } else if (map.average_park_slope > 23) {
        alert('The area you\'ve chosen for your park is a little steep. Ideally it should be between 10 and 20 deg.');
      }
      dispatch(createMapAction(map));
    });
}


