import { baseUrlBackend } from '../../constants';

export const LOGIN_USER = 'user/login';
export const LOGOUT_USER = 'user/logout';

export const loginAction = (token, username) => {
  return {
    type: LOGIN_USER,
    payload: {
      token: token,
      username: username,
    }
  }
} 

export const logoutAction = () => {
  return {
    type: LOGOUT_USER,
  }
}

// Below here is middleware.
export const loginUser = (state) => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/auth/token/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    method: 'POST',
    body: JSON.stringify({
      username: state.username,
      password: state.password,
    })
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(token => {
      dispatch(loginAction(token, state.username));
    });
}

export const fetchLocalUser = () => (dispatch) => {
  const userStr = localStorage.getItem('user');
  if (userStr) {
    const user = JSON.parse(userStr);
    dispatch(loginAction(user.token, user.username)); 
  }
}
