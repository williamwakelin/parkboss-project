import { baseUrlBackend } from '../../constants';

export const GET_PARKS = 'parks/get';
export const CREATE_PARK = 'parks/create';

export const getUserParksAction = (parks) => {
  return {
    type: GET_PARKS,
    payload: {
      parks: parks,
    }
  }
} 

export const createParkAction = (park) => {
  return {
    type: CREATE_PARK,
    payload: {
      park: park,
    }
  }
} 


// Below here is middleware.
export const getUserParks = () => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/parks/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'GET'
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(parks => {
      dispatch(getUserParksAction(parks));
    });
}

export const createPark = (name) => (dispatch, getState) =>  {
  const url = `${baseUrlBackend}/parks/new/`;

  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + getState().user.token.access,
    }),
    method: 'POST',
    body: JSON.stringify({
      name: name,
    }) 
  }

  return fetch(url, options)
    .then(result => result.json())
    .then(park => {
      dispatch(createParkAction(park));
    });
}

export const fetchLocalParks = () => (dispatch) => {
  const parksStr = localStorage.getItem('parks');
  if (parksStr) {
    const parks = JSON.parse(parksStr);
    dispatch(getUserParksAction(parks)); 
  }
}
