
export const GET_OBSTACLES = 'obstacles/get';

export const getObstaclesAction = (obstacles) => {
  return {
    type: GET_OBSTACLES,
    payload: {
      obstacles: obstacles,
    }
  }
} 

export const fetchLocalObstacles = () => (dispatch) => {
  const obstacleStr = localStorage.getItem('obstacles');
  if (obstacleStr) {
    const obstacles = JSON.parse(obstacleStr);
    dispatch(getObstaclesAction(obstacles)); 
  }
}