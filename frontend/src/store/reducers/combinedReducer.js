import { combineReducers } from 'redux';
import userReducer from './userReducer.js';
import parkReducer from './parkReducer.js';
import currentParkReducer from './currentParkReducer.js';
import mapReducer from './mapReducer.js';
import obstacleReducer from './obstacleReducer.js';


const combinedReducer = combineReducers({
  user: userReducer,
  parks: parkReducer,
  currentPark: currentParkReducer,
  map: mapReducer,
  obstacles: obstacleReducer,
});

export default combinedReducer; 