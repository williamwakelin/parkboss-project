import { GET_KICKERS, CREATE_KICKER, DELETE_KICKER } from '../actions/kickerActions.js';
import { GET_JIBS, CREATE_JIB, DELETE_JIB } from '../actions/jibActions.js';
import { GET_OBSTACLES } from '../actions/obstacleActions.js';
import { SELECT_PARK } from '../actions/selectParkActions.js';
import { LOGOUT_USER } from '../actions/loginLogoutActions.js';


const obstacleReducer = (state = [], action) => {
  switch (action.type) {
    case SELECT_PARK:
      let newState = [];
      return newState
    case GET_KICKERS:
      newState = state.concat(action.payload.kickers);
      newState.sort(compareObstacles);
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case CREATE_KICKER:
      newState = state.concat(action.payload.kicker);
      newState.sort(compareObstacles);
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case DELETE_KICKER:
      newState = [...state];
      newState.pop();
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case GET_JIBS:
      newState = state.concat(action.payload.jibs);
      newState.sort(compareObstacles);
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case CREATE_JIB:
      newState = state.concat(action.payload.jib);
      newState.sort(compareObstacles);
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case DELETE_JIB:
      newState = [...state];
      newState.pop();
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case GET_OBSTACLES:
      newState = action.payload.obstacles;
      localStorage.setItem('obstacles', JSON.stringify(newState));
      return newState;
    case LOGOUT_USER:
      newState = [];
      return newState;
    default:
    return state;
  }
}

export default obstacleReducer;

const compareObstacles = (obstacleA, obstacleB) => {
  const positionA = obstacleA.position;
  const positionB = obstacleB.position;

  let comparison = 0;
  if (positionA > positionB) {
    comparison = 1;
  } else if (positionA < positionB) {
    comparison = -1;
  }

  return comparison;
}