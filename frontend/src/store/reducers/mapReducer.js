import { GET_MAPS, CREATE_MAP } from '../actions/mapActions.js';
import { LOGOUT_USER } from '../actions/loginLogoutActions.js';

const mapReducer = (state = [], action) => {
  switch (action.type) {
    case GET_MAPS:
      let newState = action.payload.maps;
      localStorage.setItem('map', JSON.stringify(newState));
      return newState;
    case CREATE_MAP:
      newState = [action.payload.map];
      localStorage.setItem('map', JSON.stringify(newState));
      return newState;
    case LOGOUT_USER:
      newState = [];
      return newState;
    default:
    return state;
  }
}

export default mapReducer;