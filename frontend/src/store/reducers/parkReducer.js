import { GET_PARKS, CREATE_PARK } from '../actions/parkActions.js';
import { LOGOUT_USER } from '../actions/loginLogoutActions.js';

const parkReducer = (state = [], action) => {
  switch (action.type) {
    case GET_PARKS:
      let newState = action.payload.parks;
      localStorage.setItem('parks', JSON.stringify(newState));
      return newState;
    case CREATE_PARK:
      newState = state.concat([action.payload.park]);
      localStorage.setItem('parks', JSON.stringify(newState));
      return newState;
    case LOGOUT_USER:
      newState = [];
      return newState;
    default:
    return state;
  }
}

export default parkReducer;