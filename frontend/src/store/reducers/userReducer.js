import { LOGIN_USER, LOGOUT_USER } from '../actions/loginLogoutActions.js';

const userReducer = (state = {}, action) => {
  switch (action.type) {
    case LOGIN_USER:
      let newState = {...state};
      newState = action.payload;
      localStorage.setItem('user', JSON.stringify(newState));
      return newState;
    case LOGOUT_USER:
      newState = {};
      localStorage.clear();
      return newState;
    default:
    return state;
  }
}

export default userReducer;