import { SELECT_PARK } from '../actions/selectParkActions.js';
import { CREATE_PARK } from '../actions/parkActions.js';
import { LOGOUT_USER } from '../actions/loginLogoutActions.js';

const currentParkReducer = (state = 0, action) => {
  switch (action.type) {
    case SELECT_PARK:
      let newState = action.payload.currentParkId;
      localStorage.setItem('currentPark', JSON.stringify(newState));
      return newState;
    case CREATE_PARK:
      newState = action.payload.park.id;
      localStorage.setItem('currentPark', JSON.stringify(newState));
      return newState;
    case LOGOUT_USER:
      newState = 0;
      return newState;
    default:
    return state;
  }
}

export default currentParkReducer;