import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { createKicker } from '../../store/actions/kickerActions.js';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    marginRight: 22,
  },
  input: {
    display: 'none',
  },
});

function ButtonCreateKicker(props) {

  const createKickerHandler = () => {
    props.handleClose();
    props.dispatch(createKicker(props.kickerParams));
  }
  
  const { classes } = props;
  return (
    <div>
      <Button className={classes.button}
        color="primary"
        onClick={createKickerHandler}
        variant="contained"
        >
        Create
      </Button>
    </div>
  );
}

ButtonCreateKicker.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect()(ButtonCreateKicker));

