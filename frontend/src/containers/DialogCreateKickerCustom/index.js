import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonCreateKicker from '../ButtonCreateKicker';
import MenuItem from '@material-ui/core/MenuItem';


export default class DialogCreateKickerCustom extends React.Component {
  state = {
    open: false,
    type: 'custom kicker',
    length: 0,
    takeoffHeight: 0,
    takeoffAngle: 0,
    landingAngle: 0,
  };

  getKickerParams = () => {
    const kickerParams = {
      type: this.state.type,
      length: this.state.length,
      takeoff_height: this.state.takeoffHeight,
      takeoff_angle: this.state.takeoffAngle,
      landing_angle: this.state.landingAngle,
    };
    return kickerParams;
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    this.props.closeMenu();
  };

  lengthChangeHandler = (event) => {
    this.setState({
      length: parseFloat(event.currentTarget.value),
    }) 
  }

  takeoffHeightChangeHandler = (event) => {
    this.setState({
      takeoffHeight: parseFloat(event.currentTarget.value),
    }) 
  }

  takeoffAngleChangeHandler = (event) => {
    this.setState({
      takeoffAngle: parseFloat(event.currentTarget.value),
    }) 
  }

  landingAngleChangeHandler = (event) => {
    this.setState({
      landingAngle: parseFloat(event.currentTarget.value),
    }) 
  }

  render() {
    return (
      <div>
        <MenuItem onClick={this.handleClickOpen}>Custom</MenuItem>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            <div>Create Custom Kicker</div>
          </DialogTitle>
          <DialogContent>
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="length"
              placeholder="Table length in metres"
              fullWidth
              margin="normal"
              required={true}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.lengthChangeHandler}
            />
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="takeoff height"
              placeholder="Takeoff height in metres"
              fullWidth
              margin="normal"
              required="true"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.takeoffHeightChangeHandler}
            />
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="takeoff angle"
              placeholder="Takeoff angle in degrees"
              fullWidth
              margin="normal"
              required="true"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.takeoffAngleChangeHandler}
            />
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="landing angle"
              // style={{ margin: 10 }}
              placeholder="landing angle in degrees"
              fullWidth
              margin="normal"
              required="true"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.landingAngleChangeHandler}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <ButtonCreateKicker 
              kickerParams={this.getKickerParams()}
              handleClose={this.handleClose} />
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
