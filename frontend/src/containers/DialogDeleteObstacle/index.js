import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DeleteIcon from '@material-ui/icons/Delete';
import './index.css';
import ButtonDeleteObstacle from '../ButtonDeleteObstacle';


class DialogDeleteObstacle extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <div className="delete-button-icon">
          <DeleteIcon onClick={this.handleClickOpen}/>
        </div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="form-dialog-title">
            <div>Delete this obstacle?</div>
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <ButtonDeleteObstacle 
              handleClose={this.handleClose} 
              color="primary" 
              autoFocus
              obstacle={this.props.obstacle}
            >
              Delete
            </ButtonDeleteObstacle>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DialogDeleteObstacle;