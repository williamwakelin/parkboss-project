import React, { Component } from 'react';
import NavBarHome from '../NavBarHome';
import LoginForm from '../LoginForm';

import './index.css';

class Home extends Component {
  render() {
    return (
      <div className="home">
        
        <NavBarHome />
        <LoginForm />
        <span className="home-attribution">Photo by Philipp Kämmerer on Unsplash</span>
      </div>
    );
  }
}

export default Home;
