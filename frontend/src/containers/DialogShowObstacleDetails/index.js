import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogDeleteObstacle from '../DialogDeleteObstacle';
import './index.css';

class DialogShowObstacleDetails extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <li className="obstacle">
          <div className="obstacle-title" onClick={this.handleClickOpen}>
            <h4>{ this.props.obstacle.type.toUpperCase() }</h4>
            <h6>{ `${this.props.obstacle.length}m` }</h6>
          </div>
          {
            (this.props.obstacle.position === this.props.numberOfObstacles) ?
            <DialogDeleteObstacle obstacle={this.props.obstacle} /> : null
          }
        </li>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          
          <DialogTitle id="form-dialog-title">
            <div>
              {`FEATURE ${this.props.obstacle.position} | ${this.props.obstacle.type}`}
            </div>
          </DialogTitle>
          <DialogContent className="obstacle-details-list">
            <span className="obstacle-details-key">length</span>
            <span className="obstacle-details-value">{`${this.props.obstacle.length}m`}</span><br/>    
            {
            (this.props.obstacle.type.toLowerCase() === 'cheese wedge' ||
              this.props.obstacle.type.toLowerCase() === 'roller' ||
              this.props.obstacle.type.toLowerCase() === 'custom kicker') ? 
              <div>
                <span className="obstacle-details-key">takeoff height</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.takeoff_height.toFixed(1)} m`}</span><br/>
                <span className="obstacle-details-key">takeoff angle</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.takeoff_angle.toFixed(0)} deg`}</span><br/>
                <span className="obstacle-details-key">landing angle</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.landing_angle.toFixed(0)} deg`}</span><br/>
                <span className="obstacle-details-key">takeoff length</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.takeoff_length.toFixed(1)} m`}</span><br/>
                <span className="obstacle-details-key">landing length</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.landing_length.toFixed(1)} m`}</span><br/>
                <span className="obstacle-details-key">distance from previous landing</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.actual_dist_from_prev_landing.toFixed(1)} m`}</span><br/>
                <span className="obstacle-details-key">takeoff speed</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.min_takeoff_speed.toFixed(1)} km/h`}</span><br/>
                <span className="obstacle-details-key">airtime</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.min_airtime.toFixed(2)} s`}</span><br/>
              </div> :
              <div>
                <span className="obstacle-details-key">takeoff angle</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.takeoff_angle.toFixed(0)} deg`}</span><br/>
                <span className="obstacle-details-key">takeoff length</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.takeoff_length.toFixed(1)} m`}</span><br/>
                <span className="obstacle-details-key">distance from previous landing</span>
                <span className="obstacle-details-value">{`${this.props.obstacle.actual_dist_from_prev_landing.toFixed(1)} m`}</span><br/>
              </div>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
                OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect()(DialogShowObstacleDetails);
