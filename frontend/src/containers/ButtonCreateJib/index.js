import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { createJib } from '../../store/actions/jibActions.js';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    marginRight: 22,
  },
  input: {
    display: 'none',
  },
});

function ButtonCreateJib(props) {

  const createJibHandler = () => {
    props.handleClose();
    props.dispatch(createJib(props.jibParams));
  }
  const { classes } = props;
  return (
    <div>
      <Button className={classes.button}
        color="primary"
        onClick={createJibHandler}
        variant="contained"
        >
        Create
      </Button>
    </div>
  );
}

ButtonCreateJib.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect()(ButtonCreateJib));

