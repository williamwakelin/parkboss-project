import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './index.css';


const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    marginBottom: 15,
  },
};

function NavBarHome(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="fixed" style={{backgroundColor: 'black'}}>
        <Toolbar>
          <Typography variant="h2" color="inherit" className={classes.grow}>
            PARK BOSS
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

NavBarHome.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBarHome);
