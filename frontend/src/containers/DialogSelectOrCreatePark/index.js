import React from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ShowParksList from '../ShowParksList';
import ButtonCreateNewPark from '../ButtonCreateNewPark';
import './index.css';

export default class DialogSelectOrCreatePark extends React.Component {
  state = {
    open: true,
    parkName: '',
    disabled: true,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  parkNameChangeHandler = (event) => {
    let disabled = true;
    if (event.target.value !== '') {
      disabled = false;
    }
    this.setState({
      parkName: event.currentTarget.value,
      disabled: disabled,
    }) 
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title" style={{color: 'white'}}><div className="helloworld">Select a park to work with or create a new park</div></DialogTitle>
          <DialogContent>
            <ShowParksList handleClose={this.handleClose}/>
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="new park name"
              placeholder="my awesome park name"
              fullWidth
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.parkNameChangeHandler}
            />
          </DialogContent>
          <DialogActions>
            <ButtonCreateNewPark 
              disabled={this.state.disabled}
              name={this.state.parkName}
              handleClose={this.handleClose} />
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
