import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import L from 'leaflet';
import 'leaflet-draw';
import './index.css';
import { getElevations } from '../../store/actions/mapActions';

const mapBoxToken = process.env.REACT_APP_MAPBOX_TOKEN;
class Show2dMap extends Component {
  state = {
    mymap: {}
  }

  componentDidMount() {
    if (Object.keys(this.props.map).length === 0) {
      this.createNewMap();
    } else {
      this.createMapForExistingPark();
    }  
  }

  // Add created obstacles to map.
  createObstacleMarkers = (map) => {
    this.state.markerGroup.clearLayers();
    const newMarkerGroup = this.state.markerGroup;
    this.props.obstacles.map(obstacle => {
      const positionInPark = obstacle.distance_from_park_start / this.props.map[0].length;
      const obstacleLat = positionInPark * (this.props.map[0].end_latitude - this.props.map[0].start_latitude) + this.props.map[0].start_latitude;
      const obstacleLng = positionInPark * (this.props.map[0].end_longitude - this.props.map[0].start_longitude) + this.props.map[0].start_longitude;
      const marker = L.marker([obstacleLat, obstacleLng]).addTo(newMarkerGroup);
      marker.bindTooltip(`${obstacle.position} ${obstacle.type} ${obstacle.length}m`);
    })
    return newMarkerGroup;
  }

  // Display world map with draw controls and instructions so user can zoom
  // to their chosen area and draw a line to define their new park.
  createNewMap = () => {

    let mymap = L.map('mapid').setView([0, 0], 2);
    mymap.setMinZoom(2);    

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: '<a href=\'https://www.mapbox.com/about/maps/\' target=\'_blank\'>&copy; Mapbox</a> <a href=\'https://openstreetmap.org/about/\' target=\'_blank\'>&copy; OpenStreetMap</a> <a class=\'mapbox-improve-map\' href=\'https://www.mapbox.com/map-feedback/\' target=\'_blank\'>Improve this map</a>',
        maxZoom: 22,
        minZoom: 1,
        id: 'mapbox.outdoors',
        accessToken: mapBoxToken,
    }).addTo(mymap);

    mymap.createPane('popups');
    mymap.getPane('popups').style.zIndex = 500;
    let markerGroup = L.layerGroup().addTo(mymap);
    
    // Add instruction tooltip when map opens.
    const instructionsMarker = new L.marker([0, 0], { opacity: 0.01 }); //opacity may be set to zero
    instructionsMarker.bindTooltip(
      "<strong>DEFINE YOUR PARK'S LOCATION: </strong><br />Zoom to your chosen area and draw <br />a line from top to bottom of the park.", 
      {
        permanent: true, 
        className: "instructions-label", 
        offset: [0, 0],
        direction: 'top',
        });
    instructionsMarker.addTo(mymap);

    const onMapClick = (event) => {
      mymap.closeTooltip(instructionsMarker);
    }

    mymap.on('click', onMapClick);

    let drawnItems = new L.FeatureGroup();
    mymap.addLayer(drawnItems);

    let drawControl = new L.Control.Draw({
        draw: {
          polygon: false,
          marker: false,
          circle: false,
          circlemarker: false,
          rectangle: false,
        },
    });
    mymap.addControl(drawControl);

    // Create new map settings when user has drawn a line to define
    // their new park.
    const onDraw = (event) => {
      const layer = event.layer;
      const polylineJson = drawnItems.toGeoJSON()

      mymap.fitBounds(layer.getBounds(), {padding: [20, 20]});

      const parkStartLatLng = layer.getLatLngs()[0];
      const parkEndLatLng = layer.getLatLngs()[1];

      let parkLine = L.polyline([
        [parkStartLatLng.lat, parkStartLatLng.lng],
        [parkEndLatLng.lat, parkEndLatLng.lng]
      ]).addTo(mymap);

      let parkStartPopup = L.popup({
        closeButton:false,
        autoClose:false,
        closeOnEscapeKey:false,
        closeOnClick:false,
        pane:'popups'
      })
        .setLatLng(parkStartLatLng)
        .setContent("START")
        .openOn(mymap);
      
      let parkEndPopup = L.popup({
        closeButton:false,
        autoClose:false,
        closeOnEscapeKey:false,
        closeOnClick:false,
      })
        .setLatLng(parkEndLatLng)
        .setContent("FINISH")
        .openOn(mymap);

      const mapZoom = mymap.getZoom();
      const mapCenterLat = mymap.getCenter().lat;
      const mapCenterLng = mymap.getCenter().lng;

      const mapParams = {
        startLat: parkStartLatLng.lat,
        startLng: parkStartLatLng.lng,
        endLat: parkEndLatLng.lat,
        endLng: parkEndLatLng.lng,
        zoom: mapZoom,
        centerLat: mapCenterLat,
        centerLng: mapCenterLng
      }
      
      mymap.removeControl(drawControl);
      this.props.dispatch(getElevations(mapParams));
      this.setState({
        mymap: mymap,
        markerGroup: markerGroup,
      });
    }
    mymap.on('draw:created', onDraw);
    this.setState({
      mymap: mymap,
      markerGroup: markerGroup,
    });
  }
  
  // Recreate map for current park on browser refresh. 
  // (Prevent map state from being reset).
  createMapForExistingPark = () => {
    let mymap = L.map('mapid').setView(
      [this.props.map[0].center_latitude, this.props.map[0].center_longitude],
      this.props.map[0].zoom_level
    );
    mymap.setMinZoom(2); 
    let markerGroup = L.layerGroup().addTo(mymap);
   
    
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: '<a href=\'https://www.mapbox.com/about/maps/\' target=\'_blank\'>&copy; Mapbox</a> <a href=\'https://openstreetmap.org/about/\' target=\'_blank\'>&copy; OpenStreetMap</a> <a class=\'mapbox-improve-map\' href=\'https://www.mapbox.com/map-feedback/\' target=\'_blank\'>Improve this map</a>',
      maxZoom: 22,
      minZoom: 1,
      id: 'mapbox.outdoors',
      accessToken: mapBoxToken,
    }).addTo(mymap);

    mymap.createPane('popups');
    mymap.getPane('popups').style.zIndex = 500;

    let parkLine = L.polyline([
      [this.props.map[0].start_latitude, this.props.map[0].start_longitude],
      [this.props.map[0].end_latitude, this.props.map[0].end_longitude]
    ]).addTo(mymap);

    let parkStartPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
      pane:'popups'

    })
      .setLatLng([this.props.map[0].start_latitude, this.props.map[0].start_longitude])
      .setContent("START")
      .openOn(mymap);
    
    let parkEndPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
    })
      .setLatLng([this.props.map[0].end_latitude, this.props.map[0].end_longitude])
      .setContent("FINISH")
      .openOn(mymap);
    
    this.setState({
      mymap: mymap,
      markerGroup: markerGroup,
    });
  }

  updateMapNoMarkers = (mymap) => {
    let parkLine = L.polyline([
      [this.props.map[0].start_latitude, this.props.map[0].start_longitude],
      [this.props.map[0].end_latitude, this.props.map[0].end_longitude]
    ]).addTo(mymap);

    mymap.fitBounds(parkLine.getBounds(), {padding: [20, 20]});

    let parkStartPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
      pane:'popups'

    })
      .setLatLng([this.props.map[0].start_latitude, this.props.map[0].start_longitude])
      .setContent("START")
      .openOn(mymap);
    
    let parkEndPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
    })
      .setLatLng([this.props.map[0].end_latitude, this.props.map[0].end_longitude])
      .setContent("FINISH")
      .openOn(mymap);

  }

  // Restore park map when an existing park is selected and 
  // update map on obstacle creation.
  updateMapForExistingPark = (mymap) => {
    let parkLine = L.polyline([
      [this.props.map[0].start_latitude, this.props.map[0].start_longitude],
      [this.props.map[0].end_latitude, this.props.map[0].end_longitude]
    ]).addTo(mymap);

    mymap.fitBounds(parkLine.getBounds(), {padding: [20, 20]});

    let parkStartPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
      pane:'popups'

    })
      .setLatLng([this.props.map[0].start_latitude, this.props.map[0].start_longitude])
      .setContent("START")
      .openOn(mymap);
    
    let parkEndPopup = L.popup({
      closeButton:false,
      autoClose:false,
      closeOnEscapeKey:false,
      closeOnClick:false,
    })
      .setLatLng([this.props.map[0].end_latitude, this.props.map[0].end_longitude])
      .setContent("FINISH")
      .openOn(mymap);

    this.createObstacleMarkers(mymap);
  }

  render() {
    if (this.props.map.length !== 0 && (Object.keys(this.state.mymap).length !== 0)) {
      this.updateMapForExistingPark(this.state.mymap);
    }
    return (
        <div id="mapid"></div>
    );
  }
}

const mapStateToProps = state => {
  return ({
    map: state.map,
    obstacles: state.obstacles,
  }) 
}

Show2dMap.propTypes = {
  map: PropTypes.object,
  obstacles: PropTypes.array,
};

export default connect(mapStateToProps)(Show2dMap);

