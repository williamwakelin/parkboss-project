import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonCreateJib from '../ButtonCreateJib';
import MenuItem from '@material-ui/core/MenuItem';


export default class DialogCreateJibDown extends React.Component {
  state = {
    open: false,
      type: 'custom jib',
      length: 0,
      takeoffAngle: 0,
  };

  getJibParams = () => {
    const jibParams = {
      type: this.state.type,
      length: this.state.length,
      takeoff_angle: this.state.takeoffAngle,
    };
    return jibParams;
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    this.props.closeMenu();
  };

  lengthChangeHandler = (event) => {
    this.setState({
      length: parseFloat(event.currentTarget.value),
    }) 
  }

  takeoffAngleChangeHandler = (event) => {
    this.setState({
      takeoffAngle: parseFloat(event.currentTarget.value),
    }) 
  }

  render() {
    return (
      <div>
        <MenuItem onClick={this.handleClickOpen}>Custom Jib</MenuItem>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            <div>Create Custom Jib</div>
          </DialogTitle>
          <DialogContent>
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="length"
              placeholder="Jib length in metres"
              fullWidth
              margin="normal"
              required={true}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.lengthChangeHandler}
            />
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="takeoff angle"
              placeholder="Takeoff angle in degrees"
              fullWidth
              margin="normal"
              required={true}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.takeoffAngleChangeHandler}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <ButtonCreateJib 
              jibParams={this.getJibParams()}
              handleClose={this.handleClose} />
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
