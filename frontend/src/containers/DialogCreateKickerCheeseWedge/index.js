import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonCreateKicker from '../ButtonCreateKicker';
import MenuItem from '@material-ui/core/MenuItem';

const TAKEOFF_HEIGHT_TO_LENGTH = 0.15

export default class DialogCreateKickerCheeseWedge extends React.Component {
  state = {
    open: false,
    type: 'Cheese Wedge',
    length: 0,
    takeoffHeight: 0,
    takeoffAngle: 26,
    landingAngle: 32,
  };

  getKickerParams = () => {
    const kickerParams = {
      type: this.state.type,
      length: this.state.length,
      takeoff_height: this.state.takeoffHeight,
      takeoff_angle: this.state.takeoffAngle,
      landing_angle: this.state.landingAngle,
    };
    return kickerParams;
  }
  
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    this.props.closeMenu();
  };

  lengthChangeHandler = (event) => {
    const takeoffHeight = parseFloat(event.currentTarget.value) * TAKEOFF_HEIGHT_TO_LENGTH;
    this.setState({
      length: parseFloat(event.currentTarget.value),
      takeoffHeight: takeoffHeight
    }); 
  };

  render() {
    return (
      <div>
        <MenuItem onClick={this.handleClickOpen}>Cheese Wedge</MenuItem>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            <div>Create Cheese Wedge Kicker</div>
          </DialogTitle>
          <DialogContent>
            <TextField
              id="outlined-full-width"
              autoComplete="off"
              label="length"
              placeholder="Table length in metres"
              fullWidth
              margin="normal"
              required={true}
              type="number"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.lengthChangeHandler}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <ButtonCreateKicker 
              kickerParams={this.getKickerParams()}
              handleClose={this.handleClose} />
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
