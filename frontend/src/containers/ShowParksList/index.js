import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ShowPark from '../ShowPark';
import { withRouter } from 'react-router'
import { connect } from 'react-redux';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class ShowParksList extends Component {

  listClickHandler = () => {
    this.props.handleClose();
  }

  render() {
    if (this.props.parks.length === 0) {
      return (
        <div>
          <p>You don't have any parks yet...</p>
        </div>
      );
    } else {
      return (
        <div className="parks-list-container">
          <List component="nav" onClick={this.listClickHandler}>
            {
              this.props.parks.map((park) => {
                  return <ShowPark key={ park.id } 
                    park={ park } />;
              })
            }
          </List>
          <Divider />
        </div>
      );
    // }
    }
  }
}

const mapStateToProps = state => {
  return ({
    parks: state.parks,
  }) 
}

ShowParksList.propTypes = {
  parks: PropTypes.array,
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(connect(mapStateToProps)(ShowParksList)));
