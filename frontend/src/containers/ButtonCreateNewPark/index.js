import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { createPark } from '../../store/actions/parkActions.js';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    marginRight: 22,
  },
  input: {
    display: 'none',
  },
});

function ButtonCreateNewPark(props) {

  const createNewParkHandler = () => {
    props.handleClose();
    props.dispatch(createPark(props.name));
  }
  const { classes } = props;
  return (
    <div>
      <Button className={classes.button}
        color="primary"
        onClick={createNewParkHandler}
        disabled={props.disabled}
        variant="contained">Create Park</Button>
    </div>
  );
}

ButtonCreateNewPark.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect()(ButtonCreateNewPark));

