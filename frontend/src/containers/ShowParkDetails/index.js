import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './index.css';
import DialogShowObstacleDetails from '../DialogShowObstacleDetails';

const uuidv4 = require('uuid/v4');


class ShowParkDetails extends Component {

  render() {

    if (this.props.parkId === 0) {
      return (
        <div>
          <h3>No Park Selected</h3>
        </div>
      );
    } else {
      return (
        <div className="park-details-container">
          <div className="park-details-title">
            <h3>{this.props.park.name}</h3>
          </div>
          <ul className="obstacles-list">
            {
              this.props.obstacles.map((obstacle, index) => {
                  return <DialogShowObstacleDetails key={ uuidv4() }
                    index={ index } 
                    obstacle={ obstacle } 
                    numberOfObstacles={this.props.obstacles.length} />;
              })
            }
          </ul>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  let currentParkObject = {};
  if (state.currentPark !== 0) {
    currentParkObject = state.parks.find(park => park.id === state.currentPark);
  } 
  return ({
    park: currentParkObject,
    parkId: state.currentPark,
    obstacles: state.obstacles,
  }) 
}

ShowParkDetails.propTypes = {
  park: PropTypes.object,
  parkId: PropTypes.number,
  obstacles: PropTypes.array,
};

export default connect(mapStateToProps)(ShowParkDetails);
