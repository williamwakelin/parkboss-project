import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import NavBarLoggedIn from '../NavBarLoggedIn';
import ShowParkDetails from '../ShowParkDetails';
import ShowParkToolbox from '../ShowParkToolbox';
import { getUserParks } from '../../store/actions/parkActions.js';
import './index.css';
import DialogSelectOrCreatePark from '../DialogSelectOrCreatePark';
import Show2dMap from '../Show2dMap';

class ProjectOverview extends Component {
  
  componentDidMount() {
      if(Object.keys(this.props.user).length === 0){
        this.props.history.push("/")
      } 
      else {
        this.props.dispatch(getUserParks());
      }
  }

  render() {
    return (
      <div className="project-overview">
        <NavBarLoggedIn />
        {
          (this.props.currentPark === 0) ? 
            <DialogSelectOrCreatePark /> : null
        }
        <div className="project-overview-container">
          <div className="layout-left">
            <div className="park-view-container">
              <Show2dMap />
            </div>
            <div className="create-obstacles-container">
              <ShowParkToolbox />
            </div>
          </div>
          <div className="layout-right">
            <ShowParkDetails />
          </div>
        </div>        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return ({
    user: state.user,
    currentPark: state.currentPark,
  }) 
}

ProjectOverview.propTypes = {
  user: PropTypes.object,
  parks: PropTypes.array
};

export default connect(mapStateToProps)(ProjectOverview);