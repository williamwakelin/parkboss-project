import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './index.css';
import MenuCreateNewKicker from '../MenuCreateNewKicker';
import MenuCreateNewJib from '../MenuCreateNewJib';


class ShowParkToolbox extends Component {
  state = {
    disabled: true,
  }

  toggleDisabled = () => {
    if (this.props.map.length !== 0) {
      this.setState({
        disabled: false,
      });
    } else {
      this.setState({
        disabled: true,
      });
    }
    return this.state.disabled;
  }

  render() {
    let disabled = true;
    if (this.props.map.length !== 0) {
      disabled = false;
    } 
    return (
      <div className="toolbox-container">
        <MenuCreateNewKicker 
          changeState={this.props.changeState}
          disabled={disabled}
        />
        <MenuCreateNewJib 
          disabled={disabled} 
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return ({
    map: state.map,
    parks: state.parks,
  }) 
}

ShowParkToolbox.propTypes = {
  map: PropTypes.array,
  parks: PropTypes.array
};

export default connect(mapStateToProps)(ShowParkToolbox);
