import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectParkAction } from '../../store/actions/selectParkActions';
import { getMaps } from '../../store/actions/mapActions';
import { getKickers } from '../../store/actions/kickerActions';
import { getJibs } from '../../store/actions/jibActions';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import TerrainIcon from '@material-ui/icons/Terrain';


class ShowPark extends Component {
  selectParkHandler = () => {
    this.props.dispatch(selectParkAction(this.props.park.id)); 
    this.props.dispatch(getMaps(this.props.park.id));
    this.props.dispatch(getKickers(this.props.park.id));
    this.props.dispatch(getJibs(this.props.park.id));
  }

  render() {
      return (
        <ListItem button onClick={this.selectParkHandler}>
          <ListItemIcon>
            <TerrainIcon />
          </ListItemIcon>
          <ListItemText primary={this.props.park.name} />
        </ListItem>
      )
  }
}

export default connect()(ShowPark);