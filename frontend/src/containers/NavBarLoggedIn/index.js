import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router'
import { connect } from 'react-redux';
import { logoutAction } from '../../store/actions/loginLogoutActions';
import './index.css';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
    color: 'white',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};



function NavBarLoggedIn(props) {
  const logoutHandler = (event) => {
    props.dispatch(logoutAction());
    props.history.push("/")
  }
  
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="fixed" style={{backgroundColor: 'black'}}>
        <Toolbar>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            PARK BOSS
          </Typography>
          <Button color="inherit" onClick={logoutHandler}>Logout</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

NavBarLoggedIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(connect()(NavBarLoggedIn)));