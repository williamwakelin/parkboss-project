import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import DialogCreateJibDown from '../DialogCreateJibDown';
import DialogCreateJibFlat from '../DialogCreateJibFlat';
import DialogCreateJibCustom from '../DialogCreateJibCustom';
import './index.css';


class MenuCreateNewJib extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;

    return (
      <div className="create-jib-menu">
        <div className="create-jib-button">
          <Button
            aria-owns={anchorEl ? 'simple-menu' : undefined}
            aria-haspopup="true"
            color="primary"
            disabled={this.props.disabled}
            onClick={this.handleClick}
            variant="contained"
            >
            Create Jib
          </Button>
        </div>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <DialogCreateJibDown 
            closeMenu={this.handleClose}
          />
          <DialogCreateJibFlat 
            closeMenu={this.handleClose}
          />
          <DialogCreateJibCustom 
            closeMenu={this.handleClose}
          />
        </Menu>
      </div>
    );
  }
}

export default MenuCreateNewJib;
