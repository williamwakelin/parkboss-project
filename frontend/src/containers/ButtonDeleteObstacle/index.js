import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { deleteKicker } from '../../store/actions/kickerActions.js';
import { deleteJib } from '../../store/actions/jibActions.js';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    marginRight: 22,
  },
  input: {
    display: 'none',
  },
});

function ButtonDeleteObstacle(props) {
  const deleteObstacleHandler = () => {
    props.handleClose();
    if (props.obstacle.type.toLowerCase() === 'cheese wedge' ||
      props.obstacle.type.toLowerCase() === 'roller' ||
      props.obstacle.type.toLowerCase() === 'custom kicker') {
        props.dispatch(deleteKicker(props.obstacle));
    } else {
      props.dispatch(deleteJib(props.obstacle));
    }
  }
  
  const { classes } = props;
  return (
    <div>
      <Button className={classes.button}
        onClick={deleteObstacleHandler}
        variant="contained"
        >
        Delete
      </Button>
    </div>
  );
}

ButtonDeleteObstacle.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect()(ButtonDeleteObstacle));

