import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { withRouter } from 'react-router'
import { loginUser } from '../../store/actions/loginLogoutActions.js';
import Button from '@material-ui/core/Button';
import './index.css';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    height: 35,
  },
});

class LoginForm extends React.Component {
  state = {
    username: 'public',
    password: 'open8888',
  };

  submitHandler = (event) => {
    event.preventDefault();
    this.props.dispatch(loginUser(this.state)).then(() => this.props.history.push('/project'));
    this.setState({
      username: '',
      password: '',
    })
    ;
  }

  usernameChangeHandler = (event) => {
    this.setState({
      username: event.currentTarget.value,
    }) 
  }

  passwordChangeHandler = (event) => {
    this.setState({
      password: event.currentTarget.value,
    }) 
  }

  render() {
    const { classes } = this.props;

    return (
      <div className="loginform-container">
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            id="filled-name"
            label="username"
            className={classes.textField}
            value={this.state.username}
            onChange={this.usernameChangeHandler}
            margin="normal"
            variant="outlined"
            InputProps={{
              className: classes.input,
            }}
          />        
          <TextField
            id="filled-password-input"
            label="Password"
            className={classes.textField}
            value={this.state.password}
            onChange={this.passwordChangeHandler}
            type="password"
            autoComplete="current-password"
            margin="normal"
            variant="outlined"
            InputProps={{
              className: classes.input,
            }}
          />
          <Button 
            variant="outlined" 
            className={classes.button}
            onClick={this.submitHandler}>
          Log in
        </Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return ({
    user: state.user,
  }) 
}

LoginForm.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object,
};

export default withRouter(withStyles(styles)(connect(mapStateToProps)(LoginForm)));
