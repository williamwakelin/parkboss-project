import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import DialogCreateKickerCheeseWedge from '../DialogCreateKickerCheeseWedge';
import DialogCreateKickerRoller from '../DialogCreateKickerRoller';
import DialogCreateKickerCustom from '../DialogCreateKickerCustom';
import './index.css';

class MenuCreateNewKicker extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleClickOpen = () => {
    this.props.handleClickOpen();
  }

  render() {
    const { anchorEl } = this.state;

    return (
      <div className="create-kicker-menu">
        <div className="create-kicker-button">
          <Button
            aria-owns={anchorEl ? 'simple-menu' : undefined}
            aria-haspopup="true"
            color="primary"
            disabled={this.props.disabled}
            onClick={this.handleClick}
            variant="contained"
          >
            Create Kicker
          </Button>
        </div>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <DialogCreateKickerCheeseWedge 
            closeMenu={this.handleClose}
          />
          <DialogCreateKickerRoller 
            closeMenu={this.handleClose}
          />
          <DialogCreateKickerCustom 
            closeMenu={this.handleClose}
          />
        </Menu>
      </div>
    );
  }
}

export default MenuCreateNewKicker;
