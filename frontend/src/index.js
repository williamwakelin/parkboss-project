import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { store } from './store';
import './index.css';
import Home from './containers/Home';
import ProjectOverview from './containers/ProjectOverview';
import { fetchLocalUser } from './store/actions/loginLogoutActions.js';
import { fetchLocalCurrentPark } from './store/actions/selectParkActions.js';
import { fetchLocalObstacles } from './store/actions/obstacleActions.js';
import { fetchLocalParks } from './store/actions/parkActions.js';
import { fetchLocalMaps } from './store/actions/mapActions.js';
import * as serviceWorker from './serviceWorker';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import 'typeface-roboto';

store.dispatch(fetchLocalUser()) 
store.dispatch(fetchLocalParks()) 
store.dispatch(fetchLocalCurrentPark())
store.dispatch(fetchLocalMaps())
store.dispatch(fetchLocalObstacles()) 

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#139100',
    },
    secondary: {
      main: '#000000',
    },
  },
  status: {
    danger: 'orange',
  },
  typography: {
    useNextVariants: true,
  },
});

  ReactDOM.render(
    <Provider store={ store }>
      <MuiThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route exact path="/" component={ Home } />
            <Route exact path="/project" component={ ProjectOverview } />
          </Switch>
        </Router>
      </MuiThemeProvider>
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
