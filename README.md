PARK BOSS

Park Boss enables those with little experience in snowpark design
to design good parks. The app was my final project for the Full Stack Bootcamp 
at Propulsion Academy in Zurich. It was completed in 3 1/2 weeks.

DISCLAIMER: At this stage Park Boss is a prototype and should NOT be used to design
a real snowpark.

WHAT IT DOES

- Assesses the suitability of the user's chosen park terrain.
- Mathematically models park features as they are created by the user
and calculates feature dimensions, speeds and required run-in. 
- Positions features in the park so that the speed requirements for each feature will
be met.
- Provides details for the construction of the features.

WHAT NEEDS TO BE ADDED

- Error handling.
- Type checking of user inputs.
- Styling. Currently not responsive.

FUTURE DEVELOPMENT

- On-slope testing and refinement of the mathematical model.
The accuracy of the model needs to be improved in order for 
truly realistic results to be achieved.
- Ability to add multiple lines to a park, or even for the user to choose an area and 
place features at will via drag and drop.
- More detailed and visual construction details for features.
